package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.DzmHisMedicines;
import com.ruoyi.system.mapper.DzmHisMedicinesMapper;
import com.ruoyi.system.service.IDzmHisMedicinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 药品信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-22
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class DzmHisMedicinesServiceImpl implements IDzmHisMedicinesService 
{
    @Autowired
    private DzmHisMedicinesMapper dzmHisMedicinesMapper;

    /**
     * 查询药品信息
     * 
     * @param medicinesId 药品信息主键
     * @return 药品信息
     */
    @Override
    public DzmHisMedicines selectDzmHisMedicinesByMedicinesId(String medicinesId)
    {
        return dzmHisMedicinesMapper.selectDzmHisMedicinesByMedicinesId(medicinesId);
    }

    /**
     * 查询药品信息列表
     * 
     * @param dzmHisMedicines 药品信息
     * @return 药品信息
     */
    @Override
    public List<DzmHisMedicines> selectDzmHisMedicinesList(DzmHisMedicines dzmHisMedicines)
    {
        return dzmHisMedicinesMapper.selectDzmHisMedicinesList(dzmHisMedicines);
    }

    /**
     * 新增药品信息
     * 
     * @param dzmHisMedicines 药品信息
     * @return 结果
     */
    @Override
    public int insertDzmHisMedicines(DzmHisMedicines dzmHisMedicines)
    {
        dzmHisMedicines.setCreateTime(DateUtils.getNowDate());
        return dzmHisMedicinesMapper.insertDzmHisMedicines(dzmHisMedicines);
    }

    /**
     * 修改药品信息
     * 
     * @param dzmHisMedicines 药品信息
     * @return 结果
     */
    @Override
    public int updateDzmHisMedicines(DzmHisMedicines dzmHisMedicines)
    {
        dzmHisMedicines.setUpdateTime(DateUtils.getNowDate());
        return dzmHisMedicinesMapper.updateDzmHisMedicines(dzmHisMedicines);
    }

    /**
     * 批量删除药品信息
     * 
     * @param medicinesIds 需要删除的药品信息主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisMedicinesByMedicinesIds(String[] medicinesIds)
    {
        return dzmHisMedicinesMapper.deleteDzmHisMedicinesByMedicinesIds(medicinesIds);
    }

    /**
     * 删除药品信息信息
     * 
     * @param medicinesId 药品信息主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisMedicinesByMedicinesId(String medicinesId)
    {
        return dzmHisMedicinesMapper.deleteDzmHisMedicinesByMedicinesId(medicinesId);
    }
}
