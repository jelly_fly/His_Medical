package com.ruoyi.system.service;

import com.ruoyi.system.domain.DzmHisPurchase;

import java.util.List;

/**
 * 采购信息Service接口
 * 
 * @author ruoyi
 * @date 2022-06-27
 */
public interface IDzmHisPurchaseService 
{
    /**
     * 查询采购信息
     * 
     * @param purchaseId 采购信息主键
     * @return 采购信息
     */
    public DzmHisPurchase selectDzmHisPurchaseByPurchaseId(String purchaseId);

    /**
     * 查询采购信息列表
     * 
     * @param dzmHisPurchase 采购信息
     * @return 采购信息集合
     */
    public List<DzmHisPurchase> selectDzmHisPurchaseList(DzmHisPurchase dzmHisPurchase);

    /**
     * 新增采购信息
     * 
     * @param dzmHisPurchase 采购信息
     * @return 结果
     */
    public int insertDzmHisPurchase(DzmHisPurchase dzmHisPurchase);

    /**
     * 修改采购信息
     * 
     * @param dzmHisPurchase 采购信息
     * @return 结果
     */
    public int updateDzmHisPurchase(DzmHisPurchase dzmHisPurchase);

    /**
     * 批量删除采购信息
     * 
     * @param purchaseIds 需要删除的采购信息主键集合
     * @return 结果
     */
    public int deleteDzmHisPurchaseByPurchaseIds(String[] purchaseIds);

    /**
     * 删除采购信息信息
     * 
     * @param purchaseId 采购信息主键
     * @return 结果
     */
    public int deleteDzmHisPurchaseByPurchaseId(String purchaseId);
}
