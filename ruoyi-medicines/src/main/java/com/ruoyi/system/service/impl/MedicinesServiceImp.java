package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;

import com.ruoyi.system.domain.MedicinesInfo;
import com.ruoyi.system.mapper.IMedicinesMapper;
import com.ruoyi.system.service.IMedicinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class MedicinesServiceImp extends ServiceImpl<IMedicinesMapper, MedicinesInfo> implements IMedicinesService {
    @Autowired
    IMedicinesMapper mapper;

    /**
     * 查询
     * @param medicines
     * @return
     */
    @Override
    public List<MedicinesInfo> querylist(MedicinesInfo medicines) {
        return mapper.querylist(medicines);
    }

    /**
     * 删除
     * @param medicinesId
     * @return
     */
    @Override
    public Integer del(Integer medicinesId) {
        return mapper.del(medicinesId);
    }

    /**
     * 导入药品数据
     *
     * @param medicinesInfosList 数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importData(List<MedicinesInfo> medicinesInfosList, Boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(medicinesInfosList) || medicinesInfosList.size() == 0)
        {
            throw new ServiceException("导入药品数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (MedicinesInfo medicinesInfo : medicinesInfosList)
        {
            try
            {
                // 验证是否存在这个药品
                MedicinesInfo u = mapper.queryByName(medicinesInfo.getMedicinesName());
                if (StringUtils.isNull(u))
                {
                    this.insertMedicinesInfo(medicinesInfo);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、药品 " + medicinesInfo.getMedicinesName() + " 导入成功");
                }
                else if (isUpdateSupport)
                {
                    medicinesInfo.setUpdateBy(operName);
                    this.updateMedicinesByName(medicinesInfo);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、药品 " + medicinesInfo.getMedicinesName() + " 更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、药品 " + medicinesInfo.getMedicinesName() + " 已存在");
                }
            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、药品 " + medicinesInfo.getMedicinesName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    @Override
    public List<MedicinesInfo> medicinesList(MedicinesInfo medicinesInfo) {
        return mapper.querylist(medicinesInfo);
    }

    /**
     * 批量修改
     * @param medicinesInfo
     */
    private void updateMedicinesByName(MedicinesInfo medicinesInfo) {
        mapper.updateMedicines(medicinesInfo);
    }

    /**
     * 批量导入
     * @param medicinesInfo
     */
    private void insertMedicinesInfo(MedicinesInfo medicinesInfo) {
        mapper.insertMedicines(medicinesInfo);
    }


}
