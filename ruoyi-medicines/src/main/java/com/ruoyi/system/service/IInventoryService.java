package com.ruoyi.system.service;

import com.ruoyi.system.domain.BatchesOfInventoryInfo;
import com.ruoyi.system.domain.DzmHisInventory;
import com.ruoyi.system.domain.Shenhe;

import java.util.List;

public interface IInventoryService {
    List<DzmHisInventory> All(DzmHisInventory inventory);

    Integer del(Integer inventoryId);

    List<Shenhe> Allss(Shenhe batches);

    Integer delss(Integer batchesOfInventoryId);

    Integer update(Shenhe shenhe);

    Integer updates(Shenhe shenhe);
}
