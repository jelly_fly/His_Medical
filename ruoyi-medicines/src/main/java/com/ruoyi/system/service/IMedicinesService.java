package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.MedicinesInfo;


import java.util.List;

public interface IMedicinesService extends IService<MedicinesInfo> {
    List<MedicinesInfo> querylist(MedicinesInfo medicines);

    Integer del(Integer medicinesId);

    /**
     * 导入药品数据
     *
     * @param dataManagementList 数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importData(List<MedicinesInfo> dataManagementList, Boolean isUpdateSupport, String operName);

    List<MedicinesInfo> medicinesList(MedicinesInfo medicinesInfo);
}
