package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.BatchesOfInventoryInfo;
import com.ruoyi.system.domain.DzmHisMedicines;
import com.ruoyi.system.domain.DzmHisSupplier;
import com.ruoyi.system.domain.PurchaseInfo;
import com.ruoyi.system.mapper.IPurchaseMapper;
import com.ruoyi.system.service.IPurchaseService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
@DataSource(DataSourceType.SLAVE)
public class PurchaseServiceImp extends ServiceImpl<IPurchaseMapper, PurchaseInfo> implements IPurchaseService {
    @Autowired
    private IPurchaseMapper mapper;


    @Override
    public List<DzmHisMedicines> queryMedicines() {
        return mapper.queryMedicines();
    }

    @Override
    public Integer addpurchase(PurchaseInfo purchaseInfo) {
        return mapper.addpurchase(purchaseInfo);
    }

    @Override
    public List<DzmHisSupplier> getSupplier() {
        return mapper.getSupplier();
    }

    @Override
    public  String createID() {
        //生成唯一的编号
            String code = DateFormatUtils.format(new Date(), "yyyyMMddHHmmss");
            Random re = new Random();
            //生成一个随机四位数
            String random = String.valueOf(re.nextInt(9999-1000+1)+1000);
            String ordernumber = code+random;
           System.out.println(ordernumber);
            return ordernumber;

    }

    @Override
    public List<DzmHisMedicines> querylist(String[] arrs) {
        return mapper.querylist(arrs);
    }

    @Override
    public Integer addBatchesInventory(BatchesOfInventoryInfo batches) {
        return mapper.addbatchesinventory(batches);
    }

    //测试
    public static void main(String[] args) {
        //            long l=System.currentTimeMillis();
            String code = DateFormatUtils.format(new Date(), "yyyyMMddHHmmss");
            Random re = new Random();
            //生成一个随机四位数
            String random = String.valueOf(re.nextInt(9999-1000+1)+1000);
            String ordernumber = code+random;
            System.out.println(ordernumber);
    }
}
