package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.BatchesOfInventoryInfo;
import com.ruoyi.system.domain.DzmHisMedicines;
import com.ruoyi.system.domain.DzmHisSupplier;
import com.ruoyi.system.domain.PurchaseInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
@DataSource(DataSourceType.SLAVE)
public interface IPurchaseMapper extends BaseMapper<PurchaseInfo> {


    List<DzmHisMedicines> queryMedicines();

    Integer addpurchase(PurchaseInfo purchaseInfo);

    List<DzmHisSupplier> getSupplier();

    List<DzmHisMedicines> querylist(String[] arrs);

    Integer addbatchesinventory(BatchesOfInventoryInfo batches);
















}
