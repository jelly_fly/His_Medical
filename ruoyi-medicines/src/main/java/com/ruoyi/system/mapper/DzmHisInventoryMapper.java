package com.ruoyi.system.mapper;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.BatchesOfInventoryInfo;
import com.ruoyi.system.domain.DzmHisInventory;
import com.ruoyi.system.domain.Shenhe;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
@DataSource(DataSourceType.SLAVE)
public interface DzmHisInventoryMapper {
    List<DzmHisInventory> All(DzmHisInventory inventory);

    Integer del(Integer inventoryId);

    List<Shenhe> ALLss(Shenhe batches);

    Integer delss(Integer batchesOfInventoryId);

    Integer update(Shenhe shenhe);

    Integer updates(Shenhe shenhe);
}
