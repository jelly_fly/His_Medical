package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.ruoyi.system.domain.MedicinesInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface IMedicinesMapper extends BaseMapper<MedicinesInfo> {
    /**
     * 查询药品列表
     * @param medicines
     * @return
     */
    List<MedicinesInfo> querylist(MedicinesInfo medicines);

    /**
     * 通过id删除药品
     * @param medicinesId
     * @return
     */
    Integer del(Integer medicinesId);

    /**
     * 通过药品名称查询药品信息
     * @param medicinesName
     * @return
     */
    MedicinesInfo queryByName(String medicinesName);

    /**
     * 添加药品
     * @param medicinesInfo
     */
    void insertMedicines(MedicinesInfo medicinesInfo);

    /**
     * 修改药品
     * @param medicinesInfo
     */
    void updateMedicines(MedicinesInfo medicinesInfo);


}
