package com.ruoyi.system.mapper;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.DzmHisMedicines;
import com.ruoyi.system.domain.DzmHisPurchase;

import java.util.List;

/**
 * 采购信息Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-27
 */
@DataSource(DataSourceType.SLAVE)
public interface DzmHisPurchaseMapper 
{
    /**
     * 查询采购信息
     * 
     * @param purchaseId 采购信息主键
     * @return 采购信息
     */
    public DzmHisPurchase selectDzmHisPurchaseByPurchaseId(String purchaseId);

    /**
     * 查询采购信息列表
     * 
     * @param dzmHisPurchase 采购信息
     * @return 采购信息集合
     */
    public List<DzmHisPurchase> selectDzmHisPurchaseList(DzmHisPurchase dzmHisPurchase);

    /**
     * 新增采购信息
     * 
     * @param dzmHisPurchase 采购信息
     * @return 结果
     */
    public int insertDzmHisPurchase(DzmHisPurchase dzmHisPurchase);

    /**
     * 修改采购信息
     * 
     * @param dzmHisPurchase 采购信息
     * @return 结果
     */
    public int updateDzmHisPurchase(DzmHisPurchase dzmHisPurchase);

    /**
     * 删除采购信息
     * 
     * @param purchaseId 采购信息主键
     * @return 结果
     */
    public int deleteDzmHisPurchaseByPurchaseId(String purchaseId);

    /**
     * 批量删除采购信息
     * 
     * @param purchaseIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzmHisPurchaseByPurchaseIds(String[] purchaseIds);

    /**
     * 批量删除药品信息
     * 
     * @param purchaseIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzmHisMedicinesByMedicinesNumbers(String[] purchaseIds);
    
    /**
     * 批量新增药品信息
     * 
     * @param dzmHisMedicinesList 药品信息列表
     * @return 结果
     */
    public int batchDzmHisMedicines(List<DzmHisMedicines> dzmHisMedicinesList);
    

    /**
     * 通过采购信息主键删除药品信息信息
     * 
     * @param purchaseId 采购信息ID
     * @return 结果
     */
    public int deleteDzmHisMedicinesByMedicinesNumber(String purchaseId);
}
