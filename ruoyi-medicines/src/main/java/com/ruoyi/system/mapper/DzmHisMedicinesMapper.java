package com.ruoyi.system.mapper;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.DzmHisMedicines;

import java.util.List;

/**
 * 药品信息Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-22
 */
@DataSource(DataSourceType.SLAVE)
public interface DzmHisMedicinesMapper 
{
    /**
     * 查询药品信息
     * 
     * @param medicinesId 药品信息主键
     * @return 药品信息
     */
    public DzmHisMedicines selectDzmHisMedicinesByMedicinesId(String medicinesId);

    /**
     * 查询药品信息列表
     * 
     * @param dzmHisMedicines 药品信息
     * @return 药品信息集合
     */
    public List<DzmHisMedicines> selectDzmHisMedicinesList(DzmHisMedicines dzmHisMedicines);

    /**
     * 新增药品信息
     * 
     * @param dzmHisMedicines 药品信息
     * @return 结果
     */
    public int insertDzmHisMedicines(DzmHisMedicines dzmHisMedicines);

    /**
     * 修改药品信息
     * 
     * @param dzmHisMedicines 药品信息
     * @return 结果
     */
    public int updateDzmHisMedicines(DzmHisMedicines dzmHisMedicines);

    /**
     * 删除药品信息
     * 
     * @param medicinesId 药品信息主键
     * @return 结果
     */
    public int deleteDzmHisMedicinesByMedicinesId(String medicinesId);

    /**
     * 批量删除药品信息
     * 
     * @param medicinesIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzmHisMedicinesByMedicinesIds(String[] medicinesIds);
}
