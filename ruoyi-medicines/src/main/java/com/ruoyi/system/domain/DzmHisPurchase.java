package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 采购信息对象 dzm_his_purchase
 * 
 * @author ruoyi
 * @date 2022-06-27
 */
public class DzmHisPurchase extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 采购信息ID */
    private String purchaseId;

    /** 医院药品关联表：hmr_id */
    @Excel(name = "医院药品关联表：hmr_id")
    private Integer medicinesId;

    /** 批次库存ID */
    @Excel(name = "批次库存ID")
    private Integer batchesOfInventoryId;

    /** 采购数量 */
    @Excel(name = "采购数量")
    private Integer purchaseNum;

    /** 采购单位 */
    @Excel(name = "采购单位")
    private String purchaseUnit;

    /** 批发价 */
    @Excel(name = "批发价")
    private Double purchaseTradePrice;

    /** 处方价 */
    @Excel(name = "处方价")
    private Double purchasePrescriptionPrice;

    /** 采购批发总额 */
    @Excel(name = "采购批发总额")
    private Double purchaseTradeTotalAmount;


    /** 采购处方总额 */
    @Excel(name = "采购处方总额")
    private Double purchasePrescriptionTotalAmount;


    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String hmrId;

//    @Excel(name = "创建时间")
//    private Date createTime;






    /** 药品信息信息 */
    private List<DzmHisMedicines> dzmHisMedicinesList;


//    @Override
//    public Date getCreateTime() {
//        return createTime;
//    }
//
//    @Override
//    public void setCreateTime(Date createTime) {
//        this.createTime = createTime;
//    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Integer getMedicinesId() {
        return medicinesId;
    }

    public void setMedicinesId(Integer medicinesId) {
        this.medicinesId = medicinesId;
    }

    public Integer getBatchesOfInventoryId() {
        return batchesOfInventoryId;
    }

    public void setBatchesOfInventoryId(Integer batchesOfInventoryId) {
        this.batchesOfInventoryId = batchesOfInventoryId;
    }

    public Integer getPurchaseNum() {
        return purchaseNum;
    }

    public void setPurchaseNum(Integer purchaseNum) {
        this.purchaseNum = purchaseNum;
    }

    public String getPurchaseUnit() {
        return purchaseUnit;
    }

    public void setPurchaseUnit(String purchaseUnit) {
        this.purchaseUnit = purchaseUnit;
    }

    public Double getPurchaseTradePrice() {
        return purchaseTradePrice;
    }

    public void setPurchaseTradePrice(Double purchaseTradePrice) {
        this.purchaseTradePrice = purchaseTradePrice;
    }

    public Double getPurchasePrescriptionPrice() {
        return purchasePrescriptionPrice;
    }

    public void setPurchasePrescriptionPrice(Double purchasePrescriptionPrice) {
        this.purchasePrescriptionPrice = purchasePrescriptionPrice;
    }

    public Double getPurchaseTradeTotalAmount() {
        return purchaseTradeTotalAmount;
    }

    public void setPurchaseTradeTotalAmount(Double purchaseTradeTotalAmount) {
        this.purchaseTradeTotalAmount = purchaseTradeTotalAmount;
    }

    public Double getPurchasePrescriptionTotalAmount() {
        return purchasePrescriptionTotalAmount;
    }

    public void setPurchasePrescriptionTotalAmount(Double purchasePrescriptionTotalAmount) {
        this.purchasePrescriptionTotalAmount = purchasePrescriptionTotalAmount;
    }

    public String getHmrId() {
        return hmrId;
    }

    public void setHmrId(String hmrId) {
        this.hmrId = hmrId;
    }

    public List<DzmHisMedicines> getDzmHisMedicinesList() {
        return dzmHisMedicinesList;
    }

    public void setDzmHisMedicinesList(List<DzmHisMedicines> dzmHisMedicinesList) {
        this.dzmHisMedicinesList = dzmHisMedicinesList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("purchaseId", getPurchaseId())
            .append("medicinesId", getMedicinesId())
            .append("batchesOfInventoryId", getBatchesOfInventoryId())
            .append("purchaseNum", getPurchaseNum())
            .append("purchaseUnit", getPurchaseUnit())
            .append("purchaseTradePrice", getPurchaseTradePrice())
            .append("purchasePrescriptionPrice", getPurchasePrescriptionPrice())
            .append("purchaseTradeTotalAmount", getPurchaseTradeTotalAmount())
            .append("purchasePrescriptionTotalAmount", getPurchasePrescriptionTotalAmount())
            .append("createTime", getCreateTime())
            .append("hmrId", getHmrId())
            .append("dzmHisMedicinesList", getDzmHisMedicinesList())
            .toString();
    }
}
