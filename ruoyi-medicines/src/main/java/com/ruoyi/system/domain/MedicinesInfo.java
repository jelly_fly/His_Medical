package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import lombok.Data;

import java.util.Date;


/**
 * 药品信息表
 */
@Data
@TableName("dzm_his_medicines")
public class MedicinesInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.AUTO)
    private Integer medicinesId;// mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
    @Excel(name = "药品编号")
    private String medicinesNumber;// varchar(60) NOT NULL DEFAULT '' COMMENT '药品编号',
    @Excel(name = "药品名称")
    private String   medicinesName ;// ` varchar(120) NOT NULL DEFAULT '' COMMENT '药品名称',
    @Excel(name = "药品分类")
    private String   medicinesClass ;// ` varchar(50) NOT NULL DEFAULT '' COMMENT '药品分类 ',
    @Excel(name = "处方类型")
    private String   prescriptionType ;// ` varchar(50) NOT NULL DEFAULT '' COMMENT '处方类型',
    @Excel(name = "单位")
    private String   unit ;// ` varchar(50) NOT NULL DEFAULT '' COMMENT '单位（g/条）',
    @Excel(name = "换算量")
    private String   conversion ;// ` int(10) NOT NULL DEFAULT '1' COMMENT '换算量',
    @Excel(name = "关键字")
    private String   keywords ;// varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
    @Excel(name = "生产厂家")
    private String    producter ;// ` varchar(50) DEFAULT '' COMMENT '生产厂家',
    @Excel(name = "角色")
    private String   mrole ;// ` varchar(50) DEFAULT '' COMMENT '角色',
    @Excel(name = "创建时间")
    private Date createTime;
    @Excel(name = "修改时间")
    private Date updateTime;


}
