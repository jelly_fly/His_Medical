package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BatchesOfInventoryInfo;
import com.ruoyi.system.domain.DzmHisInventory;
import com.ruoyi.system.domain.DzmHisMedicines;
import com.ruoyi.system.domain.Shenhe;
import com.ruoyi.system.service.IInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/his/inventory")
public class DzmHisInventoryController extends BaseController {
   @Autowired
  private IInventoryService service;


   @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
   @GetMapping("/list")
public Object All(DzmHisInventory inventory){
      return service.All(inventory);

}

    /**
     * 删除库存信息
     * @param  inventoryId
     * @return
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @GetMapping("/del")
    public Integer dle(Integer inventoryId)
    {
        System.out.println("删除ID-------------->"+inventoryId);
        return service.del(inventoryId);
    }

//    审核查询
@PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @GetMapping("/lists")
    public Object Allss(Shenhe batches){
        return service.Allss(batches);

    }
    /**
     * 删除库存信息
     * @param  batchesOfInventoryId
     * @return
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
     @GetMapping("/delss")
     public Integer dless(Integer batchesOfInventoryId) {
         System.out.println("删除ID-------------->"+batchesOfInventoryId);
         return service.delss(batchesOfInventoryId);
    }


    /**
     * 修改药品信息
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody Shenhe shenhe)
    {
        return toAjax(service.update(shenhe));
    }

    /**
     * 修改药品信息
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @PutMapping("/updatess")
    public AjaxResult edits(@RequestBody Shenhe shenhe)
    {
        return toAjax(service.updates(shenhe));
    }





}
