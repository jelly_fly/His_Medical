//package com.ruoyi.system.controller;
//
//import com.ruoyi.common.annotation.Log;
//import com.ruoyi.common.core.controller.BaseController;
//import com.ruoyi.common.core.domain.AjaxResult;
//import com.ruoyi.common.core.page.TableDataInfo;
//import com.ruoyi.common.enums.BusinessType;
//import com.ruoyi.common.utils.poi.ExcelUtil;
//import com.ruoyi.system.domain.DzmHisPurchase;
//import com.ruoyi.system.service.IDzmHisPurchaseService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
///**
// * 采购信息Controller
// *
// * @author ruoyi
// * @date 2022-06-27
// */
//@RestController
//@RequestMapping("/system/purchase")
//public class DzmHisPurchaseController extends BaseController
//{
//    @Autowired
//    private IDzmHisPurchaseService dzmHisPurchaseService;
//
//    /**
//     * 查询采购信息列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:purchase:list')")
//    @GetMapping("/lists")
//    public TableDataInfo list(DzmHisPurchase dzmHisPurchase)
//    {
//        startPage();
//        List<DzmHisPurchase> list = dzmHisPurchaseService.selectDzmHisPurchaseList(dzmHisPurchase);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出采购信息列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:purchase:export')")
//    @Log(title = "采购信息", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, DzmHisPurchase dzmHisPurchase)
//    {
//        List<DzmHisPurchase> list = dzmHisPurchaseService.selectDzmHisPurchaseList(dzmHisPurchase);
//        ExcelUtil<DzmHisPurchase> util = new ExcelUtil<DzmHisPurchase>(DzmHisPurchase.class);
//        util.exportExcel(response, list, "采购信息数据");
//    }
//
//    /**
//     * 获取采购信息详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:purchase:query')")
//    @GetMapping(value = "/{purchaseId}")
//    public AjaxResult getInfo(@PathVariable("purchaseId") String purchaseId)
//    {
//        return AjaxResult.success(dzmHisPurchaseService.selectDzmHisPurchaseByPurchaseId(purchaseId));
//    }
//
//    /**
//     * 新增采购信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:purchase:add')")
//    @Log(title = "采购信息", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody DzmHisPurchase dzmHisPurchase)
//    {
//        return toAjax(dzmHisPurchaseService.insertDzmHisPurchase(dzmHisPurchase));
//    }
//
//    /**
//     * 修改采购信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:purchase:edit')")
//    @Log(title = "采购信息", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody DzmHisPurchase dzmHisPurchase)
//    {
//        return toAjax(dzmHisPurchaseService.updateDzmHisPurchase(dzmHisPurchase));
//    }
//
//    /**
//     * 删除采购信息
//     *
//     */
//    @PreAuthorize("@ss.hasPermi('system:purchase:remove')")
//    @Log(title = "采购信息", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{purchaseIds}")
//    public AjaxResult remove(@PathVariable String[] purchaseIds)
//    {
//        return toAjax(dzmHisPurchaseService.deleteDzmHisPurchaseByPurchaseIds(purchaseIds));
//    }
//}
