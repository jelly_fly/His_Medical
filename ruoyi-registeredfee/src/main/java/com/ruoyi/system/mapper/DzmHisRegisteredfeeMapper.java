package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DzmHisRegisteredfee;

/**
 * 挂号费用Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-21
 */
public interface DzmHisRegisteredfeeMapper 
{
    /**
     * 查询挂号费用
     * 
     * @param regId 挂号费用主键
     * @return 挂号费用
     */
    public DzmHisRegisteredfee selectDzmHisRegisteredfeeByRegId(String regId);

    /**
     * 查询挂号费用列表
     * 
     * @param dzmHisRegisteredfee 挂号费用
     * @return 挂号费用集合
     */
    public List<DzmHisRegisteredfee> selectDzmHisRegisteredfeeList(DzmHisRegisteredfee dzmHisRegisteredfee);

    /**
     * 新增挂号费用
     * 
     * @param dzmHisRegisteredfee 挂号费用
     * @return 结果
     */
    public int insertDzmHisRegisteredfee(DzmHisRegisteredfee dzmHisRegisteredfee);

    /**
     * 修改挂号费用
     * 
     * @param dzmHisRegisteredfee 挂号费用
     * @return 结果
     */
    public int updateDzmHisRegisteredfee(DzmHisRegisteredfee dzmHisRegisteredfee);

    /**
     * 删除挂号费用
     * 
     * @param regId 挂号费用主键
     * @return 结果
     */
    public int deleteDzmHisRegisteredfeeByRegId(String regId);

    /**
     * 批量删除挂号费用
     * 
     * @param regIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzmHisRegisteredfeeByRegIds(String[] regIds);
}
