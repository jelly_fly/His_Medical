package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DzmHisRegisteredfeeMapper;
import com.ruoyi.system.domain.DzmHisRegisteredfee;
import com.ruoyi.system.service.IDzmHisRegisteredfeeService;

/**
 * 挂号费用Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-21
 */
@Service
@DataSource(value = DataSourceType.SLAVE)
public class DzmHisRegisteredfeeServiceImpl implements IDzmHisRegisteredfeeService 
{
    @Autowired
    private DzmHisRegisteredfeeMapper dzmHisRegisteredfeeMapper;

    /**
     * 查询挂号费用
     * 
     * @param regId 挂号费用主键
     * @return 挂号费用
     */
    @Override
    public DzmHisRegisteredfee selectDzmHisRegisteredfeeByRegId(String regId)
    {
        return dzmHisRegisteredfeeMapper.selectDzmHisRegisteredfeeByRegId(regId);
    }

    /**
     * 查询挂号费用列表
     * 
     * @param dzmHisRegisteredfee 挂号费用
     * @return 挂号费用
     */
    @Override
    public List<DzmHisRegisteredfee> selectDzmHisRegisteredfeeList(DzmHisRegisteredfee dzmHisRegisteredfee)
    {
        return dzmHisRegisteredfeeMapper.selectDzmHisRegisteredfeeList(dzmHisRegisteredfee);
    }

    /**
     * 新增挂号费用
     * 
     * @param dzmHisRegisteredfee 挂号费用
     * @return 结果
     */
    @Override
    public int insertDzmHisRegisteredfee(DzmHisRegisteredfee dzmHisRegisteredfee)
    {
        dzmHisRegisteredfee.setCreateTime(DateUtils.getNowDate());
        return dzmHisRegisteredfeeMapper.insertDzmHisRegisteredfee(dzmHisRegisteredfee);
    }

    /**
     * 修改挂号费用
     * 
     * @param dzmHisRegisteredfee 挂号费用
     * @return 结果
     */
    @Override
    public int updateDzmHisRegisteredfee(DzmHisRegisteredfee dzmHisRegisteredfee)
    {
        return dzmHisRegisteredfeeMapper.updateDzmHisRegisteredfee(dzmHisRegisteredfee);
    }

    /**
     * 批量删除挂号费用
     * 
     * @param regIds 需要删除的挂号费用主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisRegisteredfeeByRegIds(String[] regIds)
    {
        return dzmHisRegisteredfeeMapper.deleteDzmHisRegisteredfeeByRegIds(regIds);
    }

    /**
     * 删除挂号费用信息
     * 
     * @param regId 挂号费用主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisRegisteredfeeByRegId(String regId)
    {
        return dzmHisRegisteredfeeMapper.deleteDzmHisRegisteredfeeByRegId(regId);
    }
}
