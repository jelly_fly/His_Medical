package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DzmHisHospitalMapper;
import com.ruoyi.system.domain.DzmHisHospital;
import com.ruoyi.system.service.IDzmHisHospitalService;

/**
 * HIS医院基本信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@Service
@DataSource(value = DataSourceType.SLAVE)
public class DzmHisHospitalServiceImpl implements IDzmHisHospitalService 
{
    @Autowired
    private DzmHisHospitalMapper dzmHisHospitalMapper;

    /**
     * 查询HIS医院基本信息
     * 
     * @param id HIS医院基本信息主键
     * @return HIS医院基本信息
     */
    @Override
    public DzmHisHospital selectDzmHisHospitalById(String id)
    {
        return dzmHisHospitalMapper.selectDzmHisHospitalById(id);
    }

    /**
     * 查询HIS医院基本信息列表
     * 
     * @param dzmHisHospital HIS医院基本信息
     * @return HIS医院基本信息
     */
    @Override
    public List<DzmHisHospital> selectDzmHisHospitalList(DzmHisHospital dzmHisHospital)
    {
        return dzmHisHospitalMapper.selectDzmHisHospitalList(dzmHisHospital);
    }

    /**
     * 新增HIS医院基本信息
     * 
     * @param dzmHisHospital HIS医院基本信息
     * @return 结果
     */
    @Override
    public int insertDzmHisHospital(DzmHisHospital dzmHisHospital)
    {
        dzmHisHospital.setCreateTime(DateUtils.getNowDate());
        return dzmHisHospitalMapper.insertDzmHisHospital(dzmHisHospital);
    }

    /**
     * 修改HIS医院基本信息
     * 
     * @param dzmHisHospital HIS医院基本信息
     * @return 结果
     */
    @Override
    public int updateDzmHisHospital(DzmHisHospital dzmHisHospital)
    {
        dzmHisHospital.setUpdateTime(DateUtils.getNowDate());
        return dzmHisHospitalMapper.updateDzmHisHospital(dzmHisHospital);
    }

    /**
     * 批量删除HIS医院基本信息
     * 
     * @param ids 需要删除的HIS医院基本信息主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisHospitalByIds(String[] ids)
    {
        return dzmHisHospitalMapper.deleteDzmHisHospitalByIds(ids);
    }

    /**
     * 删除HIS医院基本信息信息
     * 
     * @param id HIS医院基本信息主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisHospitalById(String id)
    {
        return dzmHisHospitalMapper.deleteDzmHisHospitalById(id);
    }
}
