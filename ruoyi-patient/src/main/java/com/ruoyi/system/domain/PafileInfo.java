package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
public class PafileInfo {

    private Integer patientId;
    @TableId
    private Integer fileId;// int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',

    private String emergencyContactName;// varchar(50) NOT NULL DEFAULT '' COMMENT '紧急联系人',

    private String emergencyContactMobile;// varchar(11) NOT NULL DEFAULT '' COMMENT '紧急联系人电话',

    private Integer emergencyContactRelation;// tinyint(2) NOT NULL DEFAULT '0' COMMENT '紧急联系人关系 1：爸爸  2：妈妈  3：儿子  4：女儿  5：亲戚  6：朋友',

    private Integer leftEarHearing;// tinyint(1) NOT NULL DEFAULT '0' COMMENT '左耳听力 1：正常  2：耳聋',

    private Integer rightEarHearing;// tinyint(1) NOT NULL DEFAULT '0' COMMENT '右耳听力 1：正常  2：耳聋',

    private String leftVision;// decimal(10,1) NOT NULL COMMENT '左眼视力',

    private String rightVision ;//decimal(10,1) NOT NULL COMMENT '右眼视力',

    private String height;// decimal(10,1) NOT NULL COMMENT '身高',

    private String weight;// decimal(10,1) NOT NULL COMMENT '体重',

    private String bloodType;// text NOT NULL COMMENT '血型 1:A 2:B 3:AB 4:O    Rh血型 1:阴性 2:阳性',

    private String personalInfo;// varchar(100) NOT NULL DEFAULT '' COMMENT '个人史',

    private String familyInfo;// varchar(100) NOT NULL DEFAULT '' COMMENT '家族史',

    private Integer typeId;//'接诊类型：0初诊，1复诊，2急诊',

    private Integer isContagious;// '是否传染，0否，1是',

    private String caseCode;//诊断编号',

    private String caseResult;// '诊断信息',

    private String doctorTips ;//'医生建议',

    private String caseTitle;//主诉

}
