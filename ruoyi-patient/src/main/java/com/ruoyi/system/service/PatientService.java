package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.PafileInfo;
import com.ruoyi.system.domain.PatientInfo;

import java.util.List;

public interface PatientService extends IService<PatientInfo> {
    List<PatientInfo> querylist(PatientInfo pi);

    PafileInfo filelist(Integer patientId);

    Integer updatePatient(PatientInfo patientInfo);

    Integer deletePatient(Integer patientId);

    Integer addPatient(PafileInfo pf);

    PatientInfo All(Integer patientId);

    List<PatientInfo> selectPatientList(PatientInfo patientInfo);

    Integer updatePafie(long patientId);

    Integer upPatient(long patientId);
}
