package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.PafileInfo;
import com.ruoyi.system.domain.PatientInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
@DataSource(value = DataSourceType.SLAVE)
public interface LPatientMapper extends BaseMapper<PatientInfo> {

    List<PatientInfo> querylist(PatientInfo pi);

    PafileInfo filelist(Integer patientId);

    Integer updatePatient(PatientInfo patientInfo);

    Integer deletePatient(Integer patientId);

    Integer addPatient(PafileInfo pf);

    PatientInfo All(Integer patientId);

    List<PatientInfo> selectPatientList(PatientInfo patientInfo);

    Integer updatePafie(long patientId);

    Integer upPatient(long patientId);
}
