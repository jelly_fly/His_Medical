package com.ruoyi.medicallist.domain;

import lombok.Data;

@Data
public class PersonalDetails {
    //患者信息
    private Integer registrationId;//订单号
    private String name;//患者姓名
    private String registrationNumber;//卡号
    private String age;//年龄
    private String birthday;//出生日期
    private String sex;//性别
    private String mobile;//手机号码
    private String isCard;//证件号码
    private String address;//地址

    private Integer amount;//应收金额
    private Integer payAmount;//实收金额
    private String  platformCode;//支付方式
    private String addtime;//收费日期

    private String departmentName;//挂号科室
    private String registeredfeeName;//接诊类型
    private Integer registeredfeeFee;//挂号费
    private String truename;//接诊医生
    private Integer amounts;//诊疗费
}
