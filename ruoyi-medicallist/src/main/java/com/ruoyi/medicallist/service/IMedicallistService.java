package com.ruoyi.medicallist.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.medicallist.domain.HisMedicallist;;import java.util.List;

public interface IMedicallistService extends IService<HisMedicallist> {

    List<HisMedicallist> selectMedicalList(HisMedicallist hm);

    HisMedicallist BackNo(Integer registrationId);

    int BackNoUpdate(Integer registrationId);
}

