package com.ruoyi.medicallist.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.medicallist.domain.HisMedicallist;
import com.ruoyi.medicallist.mapper.MedicallistMapper;
import com.ruoyi.medicallist.service.IMedicallistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@DataSource(value = DataSourceType.SLAVE)
public class MedicallistImpl extends ServiceImpl<MedicallistMapper,HisMedicallist> implements IMedicallistService{

    @Autowired
    private MedicallistMapper mapper;

    @Override
    public List<HisMedicallist> selectMedicalList(HisMedicallist hm) {
        return mapper.selectMedicalList(hm);
    }

    @Override
    public HisMedicallist BackNo(Integer registrationId) {
        return mapper.BackNo(registrationId);
    }

    @Override
    public int BackNoUpdate(Integer registrationId) {
        return mapper.BackNoUpdate(registrationId);
    }
}
