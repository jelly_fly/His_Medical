package com.ruoyi.medicallist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.medicallist.domain.PersonalDetails;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@DataSource(value = DataSourceType.SLAVE)
public interface PersonalDetailsMapper extends BaseMapper<PersonalDetails> {

    PersonalDetails PersonBackNo(Integer registrationId);

    PersonalDetails OrderBackno(Integer registrationId);

    PersonalDetails RegistrationBackno(Integer registrationId);
}
