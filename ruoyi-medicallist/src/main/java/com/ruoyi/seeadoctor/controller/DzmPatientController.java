package com.ruoyi.seeadoctor.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.seeadoctor.domain.DzmPatient;
import com.ruoyi.seeadoctor.service.IDzmPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 患者用户Controller
 * 
 * @author fyy
 * @date 2022-06-17
 */
@RestController
@RequestMapping("/sickness/patient")
public class DzmPatientController extends BaseController
{
    @Autowired
    private IDzmPatientService dzmPatientService;

    /**
     * 查询患者用户列表
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/list")
    public TableDataInfo list(DzmPatient dzmPatient)
    {
        startPage();
        List<DzmPatient> list = dzmPatientService.selectDzmPatientList(dzmPatient);
        return getDataTable(list);
    }
}
