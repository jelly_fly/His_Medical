package com.ruoyi.seeadoctor.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.seeadoctor.domain.DzmHisCareOrder;
import com.ruoyi.seeadoctor.service.IDzmHisCareOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 处方列Controller
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
@RestController
@RequestMapping("/sickness/order")
public class DzmHisCareOrderController extends BaseController
{
    @Autowired
    private IDzmHisCareOrderService dzmHisCareOrderService;

    /**
     * 查询处方列列表
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/list")
    public TableDataInfo list(DzmHisCareOrder dzmHisCareOrder)
    {
        startPage();
        List<DzmHisCareOrder> list = dzmHisCareOrderService.selectDzmHisCareOrderList(dzmHisCareOrder);
        return getDataTable(list);
    }

    /**
     * 导出处方列列表
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @Log(title = "处方列", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzmHisCareOrder dzmHisCareOrder)
    {
        List<DzmHisCareOrder> list = dzmHisCareOrderService.selectDzmHisCareOrderList(dzmHisCareOrder);
        ExcelUtil<DzmHisCareOrder> util = new ExcelUtil<DzmHisCareOrder>(DzmHisCareOrder.class);
        util.exportExcel(response, list, "处方列数据");
    }

    /**
     * 获取处方列详细信息
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(dzmHisCareOrderService.selectDzmHisCareOrderById(id));
    }

    /**
     * 新增处方列
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @Log(title = "处方列", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzmHisCareOrder dzmHisCareOrder)
    {
        int id = dzmHisCareOrderService.insertDzmHisCareOrder((dzmHisCareOrder));
        return AjaxResult.success(dzmHisCareOrder.getId());
    }

    /**
     * 修改处方列
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @Log(title = "处方列", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzmHisCareOrder dzmHisCareOrder)
    {
        return toAjax(dzmHisCareOrderService.updateDzmHisCareOrder(dzmHisCareOrder));
    }

    /**
     * 删除处方列
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @Log(title = "处方列", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(dzmHisCareOrderService.deleteDzmHisCareOrderByIds(ids));
    }
}
