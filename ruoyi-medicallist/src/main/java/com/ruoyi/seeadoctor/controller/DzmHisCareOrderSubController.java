package com.ruoyi.seeadoctor.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.seeadoctor.service.IDzmHisCareOrderSubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 开诊用药明细Controller
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
@RestController
@RequestMapping("/sickness/sub")
public class DzmHisCareOrderSubController extends BaseController
{
    @Autowired
    private IDzmHisCareOrderSubService dzmHisCareOrderSubService;


    /**
     * 新增开诊用药明细
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @Log(title = "开诊用药明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody List<Map> maps)
    {
        return toAjax(dzmHisCareOrderSubService.insertDzmHisCareOrderSub(maps));
    }

}
