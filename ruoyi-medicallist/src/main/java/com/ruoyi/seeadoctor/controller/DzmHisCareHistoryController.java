package com.ruoyi.seeadoctor.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.medicallist.domain.HisMedicallist;
import com.ruoyi.seeadoctor.domain.DzmHisCareHistory;
import com.ruoyi.seeadoctor.service.IDzmHisCareHistoryService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 历史病历Controller
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
@RestController
@RequestMapping("/sickness/history")
public class DzmHisCareHistoryController extends BaseController
{
    @Autowired
    private IDzmHisCareHistoryService dzmHisCareHistoryService;


    /**
     * 获取历史病历全部信息
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping(value = "/query/{patientId}")
    public Object list(@PathVariable Integer patientId)
    {
        return dzmHisCareHistoryService.selectDzmHisCareHistoryById(patientId);
    }

    /**
     * 获取历史病历单个详细信息
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping(value = "/{id}")
    public Object one(@PathVariable Integer id)
    {
        return dzmHisCareHistoryService.selectHistoryById(id);
    }

    /**
     * 新增历史病历
     */
//    @PreAuthorize("@ss.hasPermi('his:seeadoctor:add')")
//    @Log(title = "历史病历", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(DzmHisCareHistory dzmHisCareHistory)
//    {
//        return toAjax(dzmHisCareHistoryService.insertDzmHisCareHistory(dzmHisCareHistory));
//    }

    /**
     * 修改历史病历
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @Log(title = "历史病历", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(DzmHisCareHistory dzmHisCareHistory)
    {
        return toAjax(dzmHisCareHistoryService.updateDzmHisCareHistory(dzmHisCareHistory));
    }

}
