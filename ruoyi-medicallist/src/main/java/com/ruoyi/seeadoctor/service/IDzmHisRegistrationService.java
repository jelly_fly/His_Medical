package com.ruoyi.seeadoctor.service;

import com.ruoyi.seeadoctor.domain.DzmHisRegistration;

import java.util.List;

/**
 * 门诊挂号Service接口
 * 
 * @author fyy
 * @date 2022-06-17
 */
public interface IDzmHisRegistrationService 
{
    /**
     * 查询门诊挂号
     * 
     * @param registrationId 门诊挂号主键
     * @return 门诊挂号
     */
    public DzmHisRegistration selectDzmHisRegistrationByRegistrationId(Long registrationId);

    /**
     * 查询门诊挂号列表
     * 
     * @param dzmHisRegistration 门诊挂号
     * @return 门诊挂号集合
     */
    public List<DzmHisRegistration> selectDzmHisRegistrationList(DzmHisRegistration dzmHisRegistration);

    int updateStatus(long registrationId);

 }
