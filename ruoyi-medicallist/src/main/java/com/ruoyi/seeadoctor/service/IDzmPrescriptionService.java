package com.ruoyi.seeadoctor.service;

import com.ruoyi.seeadoctor.domain.DzmPatient;
import com.ruoyi.seeadoctor.domain.DzmPrescription;

import java.util.List;

public interface IDzmPrescriptionService {
    List<DzmPrescription> selectDzmDrugList(DzmPrescription dzmPrescription);

    List<DzmPrescription> selectDzmExamineList(DzmPrescription dzmPrescription);

    List<DzmPrescription> selectDzmPatientByMedicinesId(Long[] medicinesIds);

    List<DzmPrescription> selectDzmPatientByinsIds(Long[] insIds);
}
