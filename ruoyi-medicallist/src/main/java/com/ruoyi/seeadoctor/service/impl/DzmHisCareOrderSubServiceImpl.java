package com.ruoyi.seeadoctor.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.seeadoctor.mapper.DzmHisCareOrderSubMapper;
import com.ruoyi.seeadoctor.service.IDzmHisCareOrderSubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 开诊用药明细Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class DzmHisCareOrderSubServiceImpl implements IDzmHisCareOrderSubService
{
    @Autowired
    private DzmHisCareOrderSubMapper dzmHisCareOrderSubMapper;

    /**
     * 新增开诊用药明细
     * 
     * @param maps 开诊用药明细
     * @return 结果
     */
    @Override
    public int insertDzmHisCareOrderSub(List<Map> maps)
    {
        if(maps.get(0).get("inspectionName") == null){
            return dzmHisCareOrderSubMapper.insertDzmHisCareOrderSub(maps);
        }
        return dzmHisCareOrderSubMapper.addDzmHisCareOrderSub(maps);
    }
}
