package com.ruoyi.seeadoctor.service;

import com.ruoyi.seeadoctor.domain.DzmHisCareOrder;

import java.util.List;

/**
 * 处方列Service接口
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
public interface IDzmHisCareOrderService 
{
    /**
     * 查询处方列
     * 
     * @param id 处方列主键
     * @return 处方列
     */
    public DzmHisCareOrder selectDzmHisCareOrderById(Integer id);

    /**
     * 查询处方列列表
     * 
     * @param dzmHisCareOrder 处方列
     * @return 处方列集合
     */
    public List<DzmHisCareOrder> selectDzmHisCareOrderList(DzmHisCareOrder dzmHisCareOrder);

    /**
     * 新增处方列
     * 
     * @param dzmHisCareOrder 处方列
     * @return 结果
     */
    public int insertDzmHisCareOrder(DzmHisCareOrder dzmHisCareOrder);

    /**
     * 修改处方列
     * 
     * @param dzmHisCareOrder 处方列
     * @return 结果
     */
    public int updateDzmHisCareOrder(DzmHisCareOrder dzmHisCareOrder);

    /**
     * 批量删除处方列
     * 
     * @param ids 需要删除的处方列主键集合
     * @return 结果
     */
    public int deleteDzmHisCareOrderByIds(Integer[] ids);

    /**
     * 删除处方列信息
     * 
     * @param id 处方列主键
     * @return 结果
     */
    public int deleteDzmHisCareOrderById(Integer id);
}
