package com.ruoyi.seeadoctor.mapper;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.seeadoctor.domain.DzmPatient;
import com.ruoyi.seeadoctor.domain.DzmPrescription;

import java.util.List;

@DataSource(DataSourceType.SLAVE)
public interface DzmPrescriptionMapper {
    List<DzmPrescription> selectDzmExamineList(DzmPrescription dzmPrescription);

    List<DzmPrescription> selectDzmDrugList(DzmPrescription dzmPrescription);

    List<DzmPrescription> selectDzmPatientByMedicinesId(Long[] medicinesIds);

    List<DzmPrescription> selectDzmPatientByinsIds(Long[] insIds);
}
