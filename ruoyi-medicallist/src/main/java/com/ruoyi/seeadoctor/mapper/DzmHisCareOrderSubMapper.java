package com.ruoyi.seeadoctor.mapper;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;

import java.util.List;
import java.util.Map;

/**
 * 开诊用药明细Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
@DataSource(DataSourceType.SLAVE)
public interface DzmHisCareOrderSubMapper 
{

    /**
     * 新增开诊用药明细
     * 
     * @param maps 开诊用药明细
     * @return 结果
     */
    public int insertDzmHisCareOrderSub(List<Map> maps);

    /**
     * 新增开诊用药明细
     *
     * @param maps 开诊用药明细
     * @return 结果
     */
    public int addDzmHisCareOrderSub(List<Map> maps);

}
