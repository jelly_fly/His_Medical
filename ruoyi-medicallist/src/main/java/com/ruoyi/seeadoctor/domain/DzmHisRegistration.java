package com.ruoyi.seeadoctor.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 门诊挂号对象 dzm_his_registration
 * 
 * @author fyy
 * @date 2022-06-17
 */
public class DzmHisRegistration extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long registrationId;

    /** 患者ID */
    @Excel(name = "患者ID")
    private Integer patientId;

    /** 医生ID */
    @Excel(name = "医生ID")
    private Integer physicianId;

    /** 操作员ID */
    @Excel(name = "操作员ID")
    private Integer operatorId;

    /** 诊所ID */
    @Excel(name = "诊所ID")
    private Integer companyId;

    /** 科室ID */
    @Excel(name = "科室ID")
    private Integer departmentId;

    /** 挂号费用ID */
    @Excel(name = "挂号费用ID")
    private Integer registeredfeeId;

    /** 挂号总金额 */
    @Excel(name = "挂号总金额")
    private BigDecimal registrationAmount;

    /** 挂号编号 */
    @Excel(name = "挂号编号")
    private Long registrationNumber;

    /** 挂号状态,1为待就诊，3为已退号，2为已就诊,4为作废，5,为未付款,6，为部分支付 */
    @Excel(name = "挂号状态,1为待就诊，3为已退号，2为已就诊,4为作废，5,为未付款,6，为部分支付")
    private Integer registrationStatus;

    /** 排班主表ID */
    @Excel(name = "排班主表ID")
    private Integer schedulingId;

    /** 排班时段表ID */
    @Excel(name = "排班时段表ID")
    private Integer schedulingSubsectionId;

    /** 排班星期表ID */
    @Excel(name = "排班星期表ID")
    private Integer schedulingWeekId;

    /** 收费总表care_pkg.id */
    @Excel(name = "收费总表care_pkg.id")
    private Integer pkgId;

    /** 患者姓名 */
    private String name;

    /** 患者性别 */
    private Integer sex;

    /** 患者生日 */
    private String birthday;

    /** 患者电话 */
    private String mobile;

    /** 挂号类型 */
    private String registeredfeeName;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "挂号时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createTime;

    /** 过敏信息 */
    @Excel(name = "过敏信息")
    private String allergyInfo;

    /** 地址信息 */
    @Excel(name = "地址信息")
    private String address;

    /** 历史病历ID */
    private Long historyId;

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public String getAllergyInfo() {
        return allergyInfo;
    }

    public void setAllergyInfo(String allergyInfo) {
        this.allergyInfo = allergyInfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Long registrationId) {
        this.registrationId = registrationId;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(Integer physicianId) {
        this.physicianId = physicianId;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getRegisteredfeeId() {
        return registeredfeeId;
    }

    public void setRegisteredfeeId(Integer registeredfeeId) {
        this.registeredfeeId = registeredfeeId;
    }

    public BigDecimal getRegistrationAmount() {
        return registrationAmount;
    }

    public void setRegistrationAmount(BigDecimal registrationAmount) {
        this.registrationAmount = registrationAmount;
    }

    public Long getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(Long registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Integer getRegistrationStatus() {
        return registrationStatus;
    }

    public void setRegistrationStatus(Integer registrationStatus) {
        this.registrationStatus = registrationStatus;
    }

    public Integer getSchedulingId() {
        return schedulingId;
    }

    public void setSchedulingId(Integer schedulingId) {
        this.schedulingId = schedulingId;
    }

    public Integer getSchedulingSubsectionId() {
        return schedulingSubsectionId;
    }

    public void setSchedulingSubsectionId(Integer schedulingSubsectionId) {
        this.schedulingSubsectionId = schedulingSubsectionId;
    }

    public Integer getSchedulingWeekId() {
        return schedulingWeekId;
    }

    public void setSchedulingWeekId(Integer schedulingWeekId) {
        this.schedulingWeekId = schedulingWeekId;
    }

    public Integer getPkgId() {
        return pkgId;
    }

    public void setPkgId(Integer pkgId) {
        this.pkgId = pkgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRegisteredfeeName() {
        return registeredfeeName;
    }

    public void setRegisteredfeeName(String registeredfeeName) {
        this.registeredfeeName = registeredfeeName;
    }

    @Override
    public String toString() {
        return "DzmHisRegistration{" +
                "registrationId=" + registrationId +
                ", patientId=" + patientId +
                ", physicianId=" + physicianId +
                ", operatorId=" + operatorId +
                ", companyId=" + companyId +
                ", departmentId=" + departmentId +
                ", registeredfeeId=" + registeredfeeId +
                ", registrationAmount=" + registrationAmount +
                ", registrationNumber=" + registrationNumber +
                ", registrationStatus=" + registrationStatus +
                ", schedulingId=" + schedulingId +
                ", schedulingSubsectionId=" + schedulingSubsectionId +
                ", schedulingWeekId=" + schedulingWeekId +
                ", pkgId=" + pkgId +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", birthday='" + birthday + '\'' +
                ", mobile='" + mobile + '\'' +
                ", registeredfeeName='" + registeredfeeName + '\'' +
                ", createTime=" + createTime +
                ", allergyInfo='" + allergyInfo + '\'' +
                ", address='" + address + '\'' +
                ", historyId=" + historyId +
                '}';
    }
}
