package com.ruoyi.seeadoctor.domain;

import io.swagger.models.auth.In;
import lombok.Data;
import org.apache.ibatis.annotations.Lang;

@Data
public class DzmPrescription {
    //药品
    private Integer medicinesId;//药品ID
    private String medicinesName;//药品名称
    private String conversion;//换算量
    private String unit;//单位（g/条）
    private String inventoryNum;//库存数量
    private Integer inventoryPrescriptionPrice;//处方价

    //检查项目
    private Integer insId;//项目ID
    private String inspectionName;//项目名称
    private String clas;//项目类别
    private String unitPrice;//项目单价
    private String units;//单位(元/次)
}
