package com.ruoyi.seeadoctor.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 历史病历对象 dzm_his_care_history
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
@Data
public class DzmHisCareHistory
{
    private static final long serialVersionUID = 1L;

    private Integer id;

    /** 医院id */
    private Integer hospitalId;

    /** 医生id */
    private Integer doctorId;

    /** 患者id */
    private Integer patientId;

    /** 科室id */
    private Integer departmentId;

    /** 接诊类型：0初诊，1复诊，2急诊 */
    private Integer typeId;

    /** 是否传染，0否，1是 */
    private Integer isContagious;

    /** 发病日期 */
    @Excel(name = "发病日期", width = 30, dateFormat = "yyyy-MM-dd")
    private String caseDate;

    /** 插入时间，php时间戳 */
    private Integer addtime;

    /** 诊断编号 */
    private String caseCode;

    /** 主诉 */
    private String caseTitle;

    /** 诊断信息 */
    private String caseResult;

    /** 医生建议 */
    private String doctorTips;

    /** 备注 */
    private String memo;

    /** 门诊挂号ID */
    private Long registrationId;

    //医生名称
    private String truename;

    //患者名称
    private String name;

    //科室名称
    private String departmentName;

    //医生ID
    private String physicianId;

    //科室ID
    private String did;
}

