package com.ruoyi.seeadoctor.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 患者用户对象 dzm_patient
 * 
 * @author fyy
 * @date 2022-06-17
 */
@Data
@TableName("dzm_patient")
public class DzmPatient
{

    /** 主键 */
    @TableId("patient_id")
    private Long patientId;

    /** 所属医院、诊所 */
    @Excel(name = "所属医院、诊所")
    private Integer hospitalId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /** 微信openid */
    @Excel(name = "微信openid")
    private String openid;

    /** 患者电话 */
    @Excel(name = "患者电话")
    private String mobile;

    /**  登录密码 */
    @Excel(name = " 登录密码")
    private String password;

    /** 患者性别1男2女 */
    @Excel(name = "患者性别1男2女")
    private Integer sex;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String birthday;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String idCard;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String mobile1;

    /** 是否完善信息，0否1已完善 */
    @Excel(name = "是否完善信息，0否1已完善")
    private Integer isFinal;

    /** 最后登录ip */
    @Excel(name = "最后登录ip")
    private Integer lastLoginIp;

    /** 最后登录时间 */
    @Excel(name = "最后登录时间")
    private Integer lastLoginTime;

    /** 地址信息 */
    @Excel(name = "地址信息")
    private String address;

    /** 省区id */
    @Excel(name = "省区id")
    private Long provinceId;

    /** 市区id */
    @Excel(name = "市区id")
    private Long cityId;

    /** 县区id */
    @Excel(name = "县区id")
    private Long districtId;

    /** 过敏信息 */
    @Excel(name = "过敏信息")
    private String allergyInfo;

    /** 是否移除 0：正常 1：删除 */
    @Excel(name = "是否移除 0：正常 1：删除")
    private Integer isDel;

    /** 创建时间 */
    private Date createTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateTime;
}
