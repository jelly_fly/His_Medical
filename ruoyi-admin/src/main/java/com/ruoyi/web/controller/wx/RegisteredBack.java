package com.ruoyi.web.controller.wx;

import com.ruoyi.edu.domain.CarePkgVo;
import com.ruoyi.edu.domain.CareReFundLogVo;
import com.ruoyi.edu.domain.HisRegistrationVo;
import com.ruoyi.edu.service.CarePkgIService;
import com.ruoyi.edu.service.CareReFundLogIService;
import com.ruoyi.edu.service.HisRegistrationIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: RegisteredBack
 * @package_Name: com.ruoyi.web.controller.wx
 * @User: guohaotian
 * @Date: 2022/7/1 17:24
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/registeredBack")
public class RegisteredBack {
    @Autowired
    private HisRegistrationIService hisRegistrationIService;//挂号
    @Autowired
    private CarePkgIService carePkgIService;//总收费
    @Autowired
    private CareReFundLogIService careReFundLogIService;//退费记录

    @RequestMapping("registeredNumberBack")
    public Object registeredNumberBack(HisRegistrationVo hisRegistrationVo, CarePkgVo carePkgVo, CareReFundLogVo careReFundLogVo){
        Map map=new HashMap<String,Integer>();
//        改变挂号状态
        Integer integer = hisRegistrationIService.updateStatus(hisRegistrationVo);
        map.put("hisRegistration",integer);
//        改变总收费
        Integer integer1 = carePkgIService.updateBackStatus(carePkgVo);
        map.put("carePkg",integer1);
//        新增退款记录
        Integer integer2 = careReFundLogIService.insertReFundLog(careReFundLogVo);
        map.put("careReFundLog",integer2);
        return map;
    }
}
