package com.ruoyi.web.controller.wx;

import com.ruoyi.edu.domain.HisRegistrationVo;
import com.ruoyi.edu.service.HisRegistrationIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: HisRegistrationHandler
 * @package_Name: com.ruoyi.web.controller.wx
 * @User: guohaotian
 * @Date: 2022/7/1 16:02
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/registration")
public class HisRegistrationHandler {
    @Autowired
    private HisRegistrationIService hisRegistrationIService;

    @RequestMapping("/queryHisRegistrationByPatientId")
    public Object queryHisRegistrationByPatientId(HisRegistrationVo hisRegistrationVo){
        return hisRegistrationIService.queryHisRegistrationByPatientId(hisRegistrationVo);
    }

    @RequestMapping("/queryHisRegistrationStatus")
    public Object queryHisRegistrationStatus(HisRegistrationVo hisRegistrationVo){
        return hisRegistrationIService.queryHisRegistrationStatus(hisRegistrationVo);
    }

}
