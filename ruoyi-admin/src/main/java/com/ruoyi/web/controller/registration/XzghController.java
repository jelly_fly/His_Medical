package com.ruoyi.web.controller.registration;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.registration.domain.DeptVo;
import com.ruoyi.registration.domain.HzVo;
import com.ruoyi.registration.domain.Patients;
import com.ruoyi.registration.domain.Registration;
import com.ruoyi.registration.service.IDeptService;
import com.ruoyi.registration.service.IHzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Service;
import java.util.List;

@RestController
@RequestMapping("/ghgl/xzgh")
public class XzghController  extends BaseController {
    @Autowired
    private IHzService service;
    @Autowired
    private IDeptService deptService;

    @PreAuthorize("@ss.hasPermi('charge')")
    @RequestMapping("list")
    public Object list(HzVo hzVo){

        startPage();
        List<HzVo> list = service.list(hzVo);
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('charge')")
    @RequestMapping("dept")
    public Object list(DeptVo deptVo){
        List<DeptVo> list = deptService.list();
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('charge')")
    @RequestMapping("gh")
    public Object bg(DeptVo deptVo){
        List<DeptVo> list = deptService.bg(deptVo);
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('charge')")
    @RequestMapping("qrgh")
    public Object qrgh(DeptVo deptVo){
        //插入挂号
        int num =  deptService.qrgh(deptVo);
        int ghid =  deptVo.getRegistrationId();


        toAjax(deptService.addHistory(deptVo));

        //插入挂号费到缴费总表
        toAjax(deptService.addPkg(deptVo));

        toAjax(deptService.upsdatecheduling(deptVo));
        return num;
    }
    @PreAuthorize("@ss.hasPermi('charge')")
    @PostMapping
    public AjaxResult add(@Validated Patients patients)
    {
        return toAjax(service.insertPatients(patients));
    }


}
