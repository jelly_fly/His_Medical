package com.ruoyi.web.controller.registration;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.registration.domain.Registration;
import com.ruoyi.registration.service.IRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/ghgl/ghlb")
public class RegistrationConroller extends BaseController {
    @Resource(name = "Registration")
    private IRegistrationService service;

    @PreAuthorize("@ss.hasPermi('dean')")
    @RequestMapping("list")
    public Object list(Registration reg){
        System.out.println(reg.getRegistrationStatus());
        startPage();


        List<Registration> list = service.list(reg);
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('dean')")
    @RequestMapping("tuihao")
    public Object tuihao(String registration_number){
        Object tuihao = service.tuihao(registration_number);
        return tuihao;
    }
    @PreAuthorize("@ss.hasPermi('dean')")
    @RequestMapping("zuofei")
    public Object zuofei(String registration_number){
        Object zuofei = service.zuofei(registration_number);
        return zuofei;
    }
    @PreAuthorize("@ss.hasPermi('dean')")
    @RequestMapping("fukuan")
    public Object fukuan(String registration_number){
        Object fukuan = service.fukuan(registration_number);
        return fukuan;
    }


}
