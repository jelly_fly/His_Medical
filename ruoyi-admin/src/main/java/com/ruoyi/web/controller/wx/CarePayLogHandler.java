package com.ruoyi.web.controller.wx;

import com.ruoyi.edu.domain.CarePayLogVo;
import com.ruoyi.edu.service.CarePayLogIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CarePayLogHandler
 * @package_Name: com.ruoyi.web.controller.wx
 * @User: guohaotian
 * @Date: 2022/7/1 14:36
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/payLog")
public class CarePayLogHandler {
    @Autowired
    private CarePayLogIService carePayLogIService;

        @RequestMapping("/updateStatus")
    public Object updateStatus(CarePayLogVo carePayLogVo){
        return carePayLogIService.updateStatus(carePayLogVo);
    }
}
