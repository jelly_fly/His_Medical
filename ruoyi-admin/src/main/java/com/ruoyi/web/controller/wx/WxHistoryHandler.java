package com.ruoyi.web.controller.wx;

import com.ruoyi.edu.service.HistoryIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: WxHistoryHandler
 * @package_Name: com.ruoyi.web.controller.wx
 * @User: guohaotian
 * @Date: 2022/7/14 10:11
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("wx/history")
public class WxHistoryHandler {
    @Autowired
    private HistoryIService iService;

    @RequestMapping("/byRegistrationId")
    public Object getOne(Integer registrationId){
        return iService.historyByRegistrationId(registrationId);
    }
}
