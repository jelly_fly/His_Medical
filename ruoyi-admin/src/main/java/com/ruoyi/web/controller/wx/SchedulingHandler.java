package com.ruoyi.web.controller.wx;

import com.ruoyi.edu.domain.SchedulingVo;
import com.ruoyi.edu.service.SchedulingIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: SchedulingHandler
 * @package_Name: com.ruoyi.web.controller.wx
 * @User: guohaotian
 * @Date: 2022/6/30 8:11
 * @Description:医生排班
 * @To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/scheduling")
public class SchedulingHandler {
    @Autowired
    private SchedulingIService service;

    @RequestMapping("/queryScheduling")
    public Object queryScheduling(SchedulingVo schedulingVo){
        return service.querySchDuLing(schedulingVo);
    }

    @RequestMapping("querySchDuLingById")
    public Object querySchDuLingById(SchedulingVo schedulingVo){
        return service.querySchDuLingById(schedulingVo);
    }
}
