package com.ruoyi.web.controller.wx;

import com.ruoyi.edu.domain.*;
import com.ruoyi.edu.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: RegistrationPaymentHandler
 * @package_Name: com.ruoyi.web.controller.wx
 * @User: guohaotian
 * @Date: 2022/6/30 10:18
 * @Description:支付付款
 * @To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/payment")
public class RegistrationPaymentHandler {
    @Autowired
    private HisRegistrationIService registrationIService;//门诊挂号
    @Autowired
    private CareHistoryIService careHistoryIService;//历史病例
    @Autowired
    private CarePkgIService carePkgIService;//总收费
    @Autowired
    private CarePayLogIService carePayLogIService;//收费记录
    @Autowired
    private SchedulingIService schedulingIService;//排班表

    /**
     * @Author:guohaotian
     * @Description:挂号
     * @Param:hisRegistrationVo
     * @Param:careHistoryVo
     * @Param:carePkgVo
     * @Param:carePayLogVo
     * @Return:java.lang.Object
     * @Date:2022/7/9~8:10
     */
    @RequestMapping("/registering")
    public Object InsertRegistering(HisRegistrationVo hisRegistrationVo, CareHistoryVo careHistoryVo, CarePkgVo carePkgVo, CarePayLogVo carePayLogVo){
        Map map=new HashMap<String,Integer>();
        //        首先挂号
        Integer registration = registrationIService.CreateRegistration(hisRegistrationVo);
        map.put("registration",registration);
        //        生成病例
        careHistoryVo.setRegistrationId(hisRegistrationVo.getRegistrationId());
        careHistoryVo.setCase_date(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        Integer careHistory = careHistoryIService.createCareHistory(careHistoryVo);
        map.put("careHistory",careHistory);
//        生成总收费
        carePkgVo.setCareHistoryId(careHistoryVo.getId());
        carePkgVo.setRegistrationId(hisRegistrationVo.getRegistrationId());
        Integer carePkg = carePkgIService.insertCarePkg(carePkgVo);
        map.put("carePkg",carePkg);
//        支付收费记录
        carePayLogVo.setPkgId(carePkgVo.getId());
        Integer carePayLog = carePayLogIService.insertCarePayLog(carePayLogVo);
        map.put("carePayLog",carePayLog);
//        修改门诊挂号中的收费总表id
        HisRegistrationVo registrationVo= new HisRegistrationVo(hisRegistrationVo.getRegistrationId(),carePkgVo.getId());
        Integer updateRegistration = registrationIService.UpdateRegistration(registrationVo);
        map.put("updateRegistration",updateRegistration);
        return map;
    };

    /**
     * @Author:guohaotian
     * @Description: 挂号成功修改号源
     * @Param:SchedulingVo
     * @Return:
     * @Date:2022/7/9~8:09
     */
    @RequestMapping("/updateRegisteredFeeNum")
    public Object updateRegisteredFeeNum(SchedulingVo schedulingVo){
        return schedulingIService.updateSchDuLingById(schedulingVo);
    }
}
