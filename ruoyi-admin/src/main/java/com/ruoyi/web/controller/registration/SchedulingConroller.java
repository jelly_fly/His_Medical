package com.ruoyi.web.controller.registration;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.DzmHisScheduling;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.registration.service.impl.SchedulingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("ghgl/yspb")
public class SchedulingConroller  extends BaseController {


    @Resource(name="Scheduling")
    private SchedulingServiceImpl service;

    @PreAuthorize("@ss.hasPermi('dean')")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<DzmHisScheduling> util = new ExcelUtil<DzmHisScheduling>(DzmHisScheduling.class);
        util.importTemplateExcel(response, "用户数据");
    }

    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "用户管理", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<DzmHisScheduling> util = new ExcelUtil<DzmHisScheduling>(DzmHisScheduling.class);
        List<DzmHisScheduling> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = service.importDzmHisScheduling(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }
    @PreAuthorize("@ss.hasPermi('dean')")
    @GetMapping("/list")
    public TableDataInfo list(DzmHisScheduling dzmHisScheduling)
    {
        startPage();
        List<DzmHisScheduling> list = service.list(dzmHisScheduling);
        return getDataTable(list);
    }
    /**
     * 导出我的排班列表
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "我的排班", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzmHisScheduling dzmHisScheduling)
    {
        List<DzmHisScheduling> list = service.list(dzmHisScheduling);
        System.out.println("................"+list);
        ExcelUtil<DzmHisScheduling> util = new ExcelUtil<DzmHisScheduling>(DzmHisScheduling.class);
        util.exportExcel(response, list, "我的排班数据");
    }

    @PreAuthorize("@ss.hasPermi('dean')")
    @RequestMapping("alllist")
    public TableDataInfo alllist(DzmHisScheduling dzmHisScheduling){
        startPage();
        List<DzmHisScheduling> list = service.alllist(dzmHisScheduling);
//        System.out.println(list.get(0).getscheduling_id());
        return  getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('department') || @ss.hasPermi('chief')")
    @RequestMapping("wdpb")
    public Object wdpb(DzmHisScheduling dzmHisScheduling){
        System.out.println(dzmHisScheduling.getSubsectionDate());
        System.out.println(dzmHisScheduling.getTrueName());
        List<DzmHisScheduling> list = service.wdpb(dzmHisScheduling);
        return  list;
    }
    @PreAuthorize("@ss.hasPermi('dean')")
    @RequestMapping("getScheduling")
    public DzmHisScheduling getScheduling(Integer scheduling_id){
        return service.getScheduling(scheduling_id);
    }

    /**
     * 修改医生排班
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @PutMapping("upscheduling")
    public AjaxResult updateScheduling(@Validated @RequestBody DzmHisScheduling scheduling)
    {

        return toAjax(service.upScheduling(scheduling));
    }

}
