package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 科室对象 dzm_his_department
 *
 * @author ruoyi
 * @date 2022-06-21
 */
@Data
public class DzmHisDepartment
{
    private static final long serialVersionUID = 1L;

    /** 科室id */
    private String did;

    /** 科室名称 */
    @Excel(name = "科室名称")
    private String departmentName;

    /** 科室编号 */
    @Excel(name = "科室编号")
    private String departmentNumber;

    /** 医院id */
    @Excel(name = "医院id")
    private Long hid;

    @TableField(fill = FieldFill.INSERT)
    private Long createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;
}
