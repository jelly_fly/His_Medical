package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DzmHisDepartmentMapper;
import com.ruoyi.system.domain.DzmHisDepartment;
import com.ruoyi.system.service.IDzmHisDepartmentService;

/**
 * 科室Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-21
 */
@Service
@DataSource(value = DataSourceType.SLAVE)
public class DzmHisDepartmentServiceImpl implements IDzmHisDepartmentService 
{
    @Autowired
    private DzmHisDepartmentMapper dzmHisDepartmentMapper;

    /**
     * 查询科室
     * 
     * @param did 科室主键
     * @return 科室
     */
    @Override
    public DzmHisDepartment selectDzmHisDepartmentByDid(String did)
    {
        return dzmHisDepartmentMapper.selectDzmHisDepartmentByDid(did);
    }

    /**
     * 查询科室列表
     * 
     * @param dzmHisDepartment 科室
     * @return 科室
     */
    @Override
    public List<DzmHisDepartment> selectDzmHisDepartmentList(DzmHisDepartment dzmHisDepartment)
    {
        return dzmHisDepartmentMapper.selectDzmHisDepartmentList(dzmHisDepartment);
    }

    /**
     * 新增科室
     * 
     * @param dzmHisDepartment 科室
     * @return 结果
     */
    @Override
    public int insertDzmHisDepartment(DzmHisDepartment dzmHisDepartment)
    {
        return dzmHisDepartmentMapper.insertDzmHisDepartment(dzmHisDepartment);
    }

    /**
     * 修改科室
     * 
     * @param dzmHisDepartment 科室
     * @return 结果
     */
    @Override
    public int updateDzmHisDepartment(DzmHisDepartment dzmHisDepartment)
    {
        return dzmHisDepartmentMapper.updateDzmHisDepartment(dzmHisDepartment);
    }

    /**
     * 批量删除科室
     * 
     * @param dids 需要删除的科室主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisDepartmentByDids(String[] dids)
    {
        return dzmHisDepartmentMapper.deleteDzmHisDepartmentByDids(dids);
    }

    /**
     * 删除科室信息
     * 
     * @param did 科室主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisDepartmentByDid(String did)
    {
        return dzmHisDepartmentMapper.deleteDzmHisDepartmentByDid(did);
    }
}
