package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.springframework.context.annotation.Profile;

/**
 * 检查项目费用对象 dzm_his_inspectionfee
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@Data
public class DzmHisInspectionfee
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String insId;

    /** 添加用户id */
    @Excel(name = "添加用户id")
    private Long mid;

    /** 医院id */
    @Excel(name = "医院id")
    private Long hid;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String inspectionName;

    /** 项目类别 */
    @Excel(name = "项目类别")
    private String clas;

    /** 项目单价 */
    @Excel(name = "项目单价")
    private BigDecimal unitPrice;

    /** 项目成本 */
    @Excel(name = "项目成本")
    private BigDecimal cost;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 类别id */
    @Excel(name = "类别id")
    private Long classId;

    /** 类别id */
    @Excel(name = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    /** 类别id */
    @Excel(name = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;
}
