package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DzmHisDoctor;
import com.ruoyi.system.service.IDzmHisDoctorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 医生基本信息Controller
 * 
 * @author ruoyi
 * @date 2022-06-22
 */
@RestController
@RequestMapping("/system/doctor")
public class DzmHisDoctorController extends BaseController
{
    @Autowired
    private IDzmHisDoctorService dzmHisDoctorService;

    /**
     * 查询医生基本信息列表
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @GetMapping("/list")
    public TableDataInfo list(DzmHisDoctor dzmHisDoctor)
    {
        startPage();
        List<DzmHisDoctor> list = dzmHisDoctorService.selectDzmHisDoctorList(dzmHisDoctor);
        return getDataTable(list);
    }

    /**
     * 导出医生基本信息列表
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "医生基本信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzmHisDoctor dzmHisDoctor)
    {
        List<DzmHisDoctor> list = dzmHisDoctorService.selectDzmHisDoctorList(dzmHisDoctor);
        ExcelUtil<DzmHisDoctor> util = new ExcelUtil<DzmHisDoctor>(DzmHisDoctor.class);
        util.exportExcel(response, list, "医生基本信息数据");
    }

    /**
     * 获取医生基本信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(dzmHisDoctorService.selectDzmHisDoctorById(id));
    }

    /**
     * 新增医生基本信息
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "医生基本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzmHisDoctor dzmHisDoctor)
    {
        return toAjax(dzmHisDoctorService.insertDzmHisDoctor(dzmHisDoctor));
    }

    /**
     * 修改医生基本信息
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "医生基本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzmHisDoctor dzmHisDoctor)
    {
        return toAjax(dzmHisDoctorService.updateDzmHisDoctor(dzmHisDoctor));
    }

    /**
     * 删除医生基本信息
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "医生基本信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(dzmHisDoctorService.deleteDzmHisDoctorByIds(ids));
    }
}
