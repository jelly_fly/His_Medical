package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DzmHisPrescriptionExtrachargesMapper;
import com.ruoyi.system.domain.DzmHisPrescriptionExtracharges;
import com.ruoyi.system.service.IDzmHisPrescriptionExtrachargesService;

/**
 * 处方附加费用Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-17
 */
@Service
@DataSource(value = DataSourceType.SLAVE)
public class DzmHisPrescriptionExtrachargesServiceImpl implements IDzmHisPrescriptionExtrachargesService 
{
    @Autowired
    private DzmHisPrescriptionExtrachargesMapper dzmHisPrescriptionExtrachargesMapper;

    /**
     * 查询处方附加费用
     * 
     * @param preId 处方附加费用主键
     * @return 处方附加费用
     */
    @Override
    public DzmHisPrescriptionExtracharges selectDzmHisPrescriptionExtrachargesByPreId(String preId)
    {
        return dzmHisPrescriptionExtrachargesMapper.selectDzmHisPrescriptionExtrachargesByPreId(preId);
    }

    /**
     * 查询处方附加费用列表
     * 
     * @param dzmHisPrescriptionExtracharges 处方附加费用
     * @return 处方附加费用
     */
    @Override
    public List<DzmHisPrescriptionExtracharges> selectDzmHisPrescriptionExtrachargesList(DzmHisPrescriptionExtracharges dzmHisPrescriptionExtracharges)
    {
        return dzmHisPrescriptionExtrachargesMapper.selectDzmHisPrescriptionExtrachargesList(dzmHisPrescriptionExtracharges);
    }

    /**
     * 新增处方附加费用
     * 
     * @param dzmHisPrescriptionExtracharges 处方附加费用
     * @return 结果
     */
    @Override
    public int insertDzmHisPrescriptionExtracharges(DzmHisPrescriptionExtracharges dzmHisPrescriptionExtracharges)
    {
        return dzmHisPrescriptionExtrachargesMapper.insertDzmHisPrescriptionExtracharges(dzmHisPrescriptionExtracharges);
    }

    /**
     * 修改处方附加费用
     * 
     * @param dzmHisPrescriptionExtracharges 处方附加费用
     * @return 结果
     */
    @Override
    public int updateDzmHisPrescriptionExtracharges(DzmHisPrescriptionExtracharges dzmHisPrescriptionExtracharges)
    {
        return dzmHisPrescriptionExtrachargesMapper.updateDzmHisPrescriptionExtracharges(dzmHisPrescriptionExtracharges);
    }

    /**
     * 批量删除处方附加费用
     * 
     * @param preIds 需要删除的处方附加费用主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisPrescriptionExtrachargesByPreIds(String[] preIds)
    {
        return dzmHisPrescriptionExtrachargesMapper.deleteDzmHisPrescriptionExtrachargesByPreIds(preIds);
    }

    /**
     * 删除处方附加费用信息
     * 
     * @param preId 处方附加费用主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisPrescriptionExtrachargesByPreId(String preId)
    {
        return dzmHisPrescriptionExtrachargesMapper.deleteDzmHisPrescriptionExtrachargesByPreId(preId);
    }
}
