package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CareHistory
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/6/30 15:37
 * @Description:历史病例
 * @To change this template use File | Settings | File Templates.
 */
@Data
@TableName("dzm_his_care_history")
@Alias("careHistory")
public class CareHistoryVo {
    @TableId(type = IdType.AUTO)
    private Integer id;//	int(10) unsigned		NO	是
    private Integer hospitalId;//	int(10) unsigned	0	YES		医院id
    private Integer doctorId;//	int(10) unsigned	0	YES		医生id
    private Integer patientId;//	int(10) unsigned	0	YES		患者id
    private Integer departmentId;//	int(10) unsigned	0	YES		科室id
    private Integer typeId;//	tinyint(1) unsigned	0	YES		接诊类型：0初诊，1复诊，2急诊
    private Integer isContagious;//	tinyint(1) unsigned	0	YES		是否传染，0否，1是
    private String case_date;//	date		YES		发病日期
    @TableField(fill = FieldFill.INSERT)
    private Long addtime;//	int(10) unsigned	0	YES		插入时间，php时间戳
    private String caseCode;//	varchar(32)		YES		诊断编号
    private String caseTitle;//	varchar(255)		YES		主诉
    private String caseresult;//	text		YES		诊断信息
    private String doctorTips;//	text		YES		医生建议
    private String memo;//	text		YES		备注
    private Integer registrationId;//	int		NO		门诊挂号id

    public CareHistoryVo(Integer id, Integer hospitalId, Integer doctorId, Integer patientId, Integer departmentId, Integer typeId, Integer isContagious, String case_date, Long addtime, String caseCode, String caseTitle, String caseresult, String doctorTips, String memo, Integer registrationId) {
        this.id = id;
        this.hospitalId = hospitalId;
        this.doctorId = doctorId;
        this.patientId = patientId;
        this.departmentId = departmentId;
        this.typeId = typeId;
        this.isContagious = isContagious;
        this.case_date = case_date;
        this.addtime = addtime;
        this.caseCode = caseCode;
        this.caseTitle = caseTitle;
        this.caseresult = caseresult;
        this.doctorTips = doctorTips;
        this.memo = memo;
        this.registrationId = registrationId;
    }

    public CareHistoryVo() {
    }
}
