package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: Scheduling
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/6/29 16:58
 * @Description:排班表
 * @To change this template use File | Settings | File Templates.
 */

@TableName("dzm_his_scheduling")
@Data
@Alias("scheduling")
public class SchedulingVo {
    @TableId(type = IdType.AUTO)
    private Integer schedulingId;// BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    private Integer physicianid;// INT(10) NOT NULL COMMENT '医生ID',
    private Integer schedulingType;// INT(10) NOT NULL COMMENT '排班类型 1-普通医生   2-主任医生',
    private Integer departmentId;// INT(10) NOT NULL COMMENT '科室ID',
    private Integer companyId;// INT(10) NOT NULL COMMENT '诊所ID',
    private String startTimeThisWeek;// VARCHAR(50) NOT NULL COMMENT '本周开始时间',
    private String endTimeThisWeek;// VARCHAR(50) NOT NULL COMMENT '本周结束时间',
    private String subsectionDate;// VARCHAR(255) NOT NULL COMMENT '排班日期',
    private Integer subsectionType;// INT(2) NOT NULL COMMENT '每天的时段：上午：1；下午：2；晚上：3；',
    private Integer subsectionTime;// INT(2) NOT NULL COMMENT '每天的时间：8:00-12:00：1；14:30-17:30：2；17:30-8:00：3；8:30-11:30：4',
    @TableField(update = "%s-1")
    private Integer registeredfeeNum;// INT(10) NOT NULL DEFAULT 32 COMMENT '号源数量',
    private Integer registeredfeeId;// INT(10) NOT NULL COMMENT '挂号费用ID',
    private String createTime;// VARCHAR(50) NOT NULL COMMENT '创建时间',

    private String updateTime;// VARCHAR(50) DEFAULT NULL COMMENT '更新时间',

    @TableField(exist = false)
    private DepartmentVo departmentVo;//科室

    @TableField(exist = false)
    private DoctorVo doctorVo;//医生

    @TableField(exist = false)
    private RegisteredFeeVo registeredFeeVo;//挂号费用表

    public SchedulingVo(Integer schedulingId, Integer physicianid, Integer schedulingType, Integer departmentId, Integer companyId, String startTimeThisWeek, String endTimeThisWeek, String subsectionDate, Integer subsectionType, Integer subsectionTime, Integer registeredfeeNum, Integer registeredfeeId, String createTime, String updateTime) {
        this.schedulingId = schedulingId;
        this.physicianid = physicianid;
        this.schedulingType = schedulingType;
        this.departmentId = departmentId;
        this.companyId = companyId;
        this.startTimeThisWeek = startTimeThisWeek;
        this.endTimeThisWeek = endTimeThisWeek;
        this.subsectionDate = subsectionDate;
        this.subsectionType = subsectionType;
        this.subsectionTime = subsectionTime;
        this.registeredfeeNum = registeredfeeNum;
        this.registeredfeeId = registeredfeeId;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public SchedulingVo() {
    }
}
