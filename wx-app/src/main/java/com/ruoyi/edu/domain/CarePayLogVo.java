package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CarePaylogVo
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/6/30 16:05
 * @Description:支付收费记录
 * @To change this template use File | Settings | File Templates.
 */
@TableName("dzm_his_care_paylog")
@Alias("carePayLog")
@Data
public class CarePayLogVo {
    @TableId(type = IdType.AUTO)
    private Integer id;//	int(10) unsigned		NO	是
    private Integer pkgId;//	int(10) unsigned		YES		care_pkg.id
    private String platformCode;//	varchar(128)		YES		支付平台交易单号
    private Integer paymentPlatform;//	smallint(5) unsigned	0	YES		支付方式：0现金，1微信，2支付宝，3，4，5....
    private Double payAmount;//	decimal(10,2)	0.00	YES		支付金额
    private Integer status;//	tinyint(1) unsigned	0	YES		状态，0未支付，1已支付
    @TableField(fill = FieldFill.INSERT)
    private Long addtime;//	int(10) unsigned	0	YES		添加时间

    public CarePayLogVo(Integer id, Integer pkgId, String platformCode, Integer paymentPlatform, Double payAmount, Integer status, Long addtime) {
        this.id = id;
        this.pkgId = pkgId;
        this.platformCode = platformCode;
        this.paymentPlatform = paymentPlatform;
        this.payAmount = payAmount;
        this.status = status;
        this.addtime = addtime;
    }

    public CarePayLogVo() {
    }
}
