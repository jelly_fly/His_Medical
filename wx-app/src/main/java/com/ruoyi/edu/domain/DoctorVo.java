package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: DoctorVo
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/6/29 17:04
 * @Description:医生信息表
 * @To change this template use File | Settings | File Templates.
 */
@TableName("dzm_his_doctor")
@Alias("doctor")
@Data
public class DoctorVo {
    private Integer id;//	int(11) unsigned		NO	是	用户id
    private String trueName;//	varchar(20)		NO		用户个人资料真实姓名
    private Integer age;//	int(3)	0	YES		年龄
    private String picture;//	varchar(255)		YES		头像
    private Integer sex;//	tinyint(1)	0	NO		性别 0,空1:男 2:女
    private Integer background;//	tinyint(1)	0	NO		学历 1：专科 2：本科 3：研究生 4：博士 5：博士后
    private String phone;//	varchar(11)	0	NO		手机号
    private String mailbox;//	varchar(50)		NO		邮箱
    private String strong;//	varchar(255)		NO		擅长
    private String honor;//	varchar(255)		NO		荣誉
    private String introduction;//	text		NO		简介
    private Date createTime;//	int(10)		NO		注册时间
    private Date updateTime;//	int(10)		NO		修改时间
    private Integer uid;//	int(11)		NO		用户表userid
    private Double askPrice;//	decimal(10,2)	0.00	YES		咨询价格

    public DoctorVo(Integer id, String trueName, Integer age, String picture, Integer sex, Integer background, String phone, String mailbox, String strong, String honor, String introduction, Date createTime, Date updateTime, Integer uid, Double askPrice) {
        this.id = id;
        this.trueName = trueName;
        this.age = age;
        this.picture = picture;
        this.sex = sex;
        this.background = background;
        this.phone = phone;
        this.mailbox = mailbox;
        this.strong = strong;
        this.honor = honor;
        this.introduction = introduction;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.uid = uid;
        this.askPrice = askPrice;
    }

    public DoctorVo() {
    }
}
