package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: HistoryVo
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/6/30 9:59
 * @Description:病例Vo
 * @To change this template use File | Settings | File Templates.
 */

@TableName("dzm_his_care_history")
@Data
@Alias("history")
public class HistoryVo {
    @TableId(type = IdType.AUTO)
    private Integer id;//	int(10) unsigned		NO	是
    private Integer hospitalId;//	int(10) unsigned	0	YES		医院id
    private Integer doctorId;//	int(10) unsigned	0	YES		医生id
    private Integer patientId;//	int(10) unsigned	0	YES		患者id
    private Integer departmentId;//	int(10) unsigned	0	YES		科室id
    private Integer typeId;//	tinyint(1) unsigned	0	YES		接诊类型：0初诊，1复诊，2急诊
    private Integer isContagious;//	tinyint(1) unsigned	0	YES		是否传染，0否，1是
    private String caseDate;//	date		YES		发病日期
    private Integer addtime;//	int(10) unsigned	0	YES		插入时间，php时间戳
    private String caseCode;//	varchar(32)		YES		诊断编号
    private String caseTitle;//	varchar(255)		YES		主诉
    private String caseResult;//	text		YES		诊断信息
    private String doctorTips;//	text		YES		医生建议
    private String memo;//	text		YES		备注

    public HistoryVo(Integer id, Integer hospitalId, Integer doctorId, Integer patientId, Integer departmentId, Integer typeId, Integer isContagious, String caseDate, Integer addtime, String caseCode, String caseTitle, String caseResult, String doctorTips, String memo) {
        this.id = id;
        this.hospitalId = hospitalId;
        this.doctorId = doctorId;
        this.patientId = patientId;
        this.departmentId = departmentId;
        this.typeId = typeId;
        this.isContagious = isContagious;
        this.caseDate = caseDate;
        this.addtime = addtime;
        this.caseCode = caseCode;
        this.caseTitle = caseTitle;
        this.caseResult = caseResult;
        this.doctorTips = doctorTips;
        this.memo = memo;
    }

    public HistoryVo() {
    }
}
