package com.ruoyi.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.edu.domain.DepartmentVo;
import com.ruoyi.edu.mapper.DepartmentMapper;
import com.ruoyi.edu.service.DepartmentIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: DepartmentServiceImpl
 * @package_Name: com.ruoyi.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/6/28 9:32
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@Service
@DataSource(DataSourceType.SLAVE)
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper,DepartmentVo> implements DepartmentIService {
    @Autowired
    private DepartmentMapper mapper;

    /**
     * @Author:guohaotian
     * @Description: 查询所有科室
     * @Param:null
     * @Return:
     * @Date:2022/6/28~9:41
     */
    @Override
    public List<DepartmentVo> findAll() {
        return mapper.selectList(new QueryWrapper<DepartmentVo>());
    }
}
