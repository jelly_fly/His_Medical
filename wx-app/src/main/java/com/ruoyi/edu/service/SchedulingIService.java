package com.ruoyi.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.edu.domain.SchedulingVo;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: SchedulingIService
 * @package_Name: com.ruoyi.edu.service
 * @User: guohaotian
 * @Date: 2022/6/29 17:40
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

public interface SchedulingIService extends IService<SchedulingVo> {
    List<SchedulingVo> querySchDuLing(SchedulingVo schedulingVo);
    SchedulingVo querySchDuLingById(SchedulingVo schedulingVo);
    Integer updateSchDuLingById(SchedulingVo schedulingVo);
}
