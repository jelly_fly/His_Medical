package com.ruoyi.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.edu.domain.CarePayLogVo;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CarePayLogIService
 * @package_Name: com.ruoyi.edu.service
 * @User: guohaotian
 * @Date: 2022/6/30 16:11
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

public interface CarePayLogIService extends IService<CarePayLogVo> {
    public Integer insertCarePayLog(CarePayLogVo carePayLogVo);
    public Integer updateStatus(CarePayLogVo carePayLogVo);
}
