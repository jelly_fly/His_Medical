package com.ruoyi.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.edu.domain.HisRegistrationVo;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: HisRegistrationIService
 * @package_Name: com.ruoyi.edu.service
 * @User: guohaotian
 * @Date: 2022/6/30 14:47
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

public interface HisRegistrationIService extends IService<HisRegistrationVo> {
    public Integer UpdateRegistration(HisRegistrationVo hisRegistrationVo);
    public Integer CreateRegistration(HisRegistrationVo hisRegistrationVo);
    public List<HisRegistrationVo> queryHisRegistrationByPatientId(HisRegistrationVo hisRegistrationVo);
    public List<HisRegistrationVo> queryHisRegistrationStatus(HisRegistrationVo hisRegistrationVo);
    public Integer updateStatus(HisRegistrationVo hisRegistrationVo);
}
