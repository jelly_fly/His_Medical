package com.ruoyi.edu.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.edu.domain.CareHistoryVo;
import com.ruoyi.edu.mapper.CareHistoryMapper;
import com.ruoyi.edu.service.CareHistoryIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CareHistoryServiceImpl
 * @package_Name: com.ruoyi.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/6/30 15:49
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class CareHistoryServiceImpl extends ServiceImpl<CareHistoryMapper,CareHistoryVo> implements CareHistoryIService {
    @Autowired
    private CareHistoryMapper mapper;

    /**
     * @Author:guohaotian
     * @Description:挂完号后新增病例
     * @Param:historyVo
     * @Return:java.lang.Integer
     * @Date:2022/6/30~15:52
     */
    @Override
    public Integer createCareHistory(CareHistoryVo historyVo) {
        return mapper.insert(historyVo);
    }
}
