package com.ruoyi.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.edu.domain.SchedulingVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: SchedulingMapper
 * @package_Name: com.ruoyi.edu.mapper
 * @User: guohaotian
 * @Date: 2022/6/29 17:39
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@Mapper
public interface SchedulingMapper extends BaseMapper<SchedulingVo> {
    List<SchedulingVo> querySchDuLing(SchedulingVo schedulingVo);
    SchedulingVo querySchDuLingById(SchedulingVo schedulingVo);
}
