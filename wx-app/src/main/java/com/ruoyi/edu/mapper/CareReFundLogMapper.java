package com.ruoyi.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.edu.domain.CareReFundLogVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CareReFundLogMapper
 * @package_Name: com.ruoyi.edu.mapper
 * @User: guohaotian
 * @Date: 2022/7/1 17:01
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@Mapper
public interface CareReFundLogMapper extends BaseMapper<CareReFundLogVo> {
}
