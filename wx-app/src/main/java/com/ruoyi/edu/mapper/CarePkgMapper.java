package com.ruoyi.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.edu.domain.CarePkgVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CarePkgMapper
 * @package_Name: com.ruoyi.edu.mapper
 * @User: guohaotian
 * @Date: 2022/6/30 16:00
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@Mapper
public interface CarePkgMapper extends BaseMapper<CarePkgVo> {
    List<CarePkgVo> queryCarePkgById(CarePkgVo carePkgVo);
}
