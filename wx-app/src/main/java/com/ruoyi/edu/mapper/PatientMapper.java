package com.ruoyi.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.edu.domain.PatientVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: PatientMapper
 * @package_Name: com.sykj.edu.mapper
 * @User: guohaotian
 * @Date: 2022/6/18 9:03
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@Mapper
public interface PatientMapper extends BaseMapper<PatientVo> {
    Integer delPatient(PatientVo patientVo);
}
