package com.ruoyi.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.edu.domain.CarePayLogVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CarePayLogMapper
 * @package_Name: com.ruoyi.edu.mapper
 * @User: guohaotian
 * @Date: 2022/6/30 16:10
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@Mapper
public interface CarePayLogMapper extends BaseMapper<CarePayLogVo> {
}
