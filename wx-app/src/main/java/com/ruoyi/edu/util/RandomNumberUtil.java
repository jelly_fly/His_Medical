package com.ruoyi.edu.util;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: RandomNumberUtil
 * @package_Name: com.ruoyi.edu.util
 * @User: guohaotian
 * @Date: 2022/6/30 14:26
 * @Description:生成时间戳加4为位随机小数
 * @To change this template use File | Settings | File Templates.
 */

@Component
public class RandomNumberUtil {
    @Bean
    public  Long getRandomNumber(){
        Long l = System.currentTimeMillis();
        Long v =(long)(Math.random() * (9999-1000)+1000);
        Long result=Long.parseLong(l.toString()+v.toString());
        return result;
    }

    @Bean
    public Long getCurrentTimeMillis(){
        return System.currentTimeMillis()/1000;
    }
}
