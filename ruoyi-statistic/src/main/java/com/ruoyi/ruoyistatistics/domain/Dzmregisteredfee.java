package com.ruoyi.ruoyistatistics.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
@TableName("dzm_his_registeredfee")
public class Dzmregisteredfee {
       //挂号费用名称
       @Excel(name = "挂号费用名称")
       private String name;
       //金额
       @Excel(name = "金额")
       private Double fee;
       //挂号费用子名称
       @Excel(name = "挂号费用子名称")
       private String names;
       //子费用
       @Excel(name = "子费用")
       private Double fees;
       //子费用数量
       @Excel(name = "子费用数量")
       private Integer sub;
       //子费用总数
       @Excel(name = "子费用总数")
       private Double stered;
       //挂号费用总金额
       @Excel(name = "挂号费用总金额")
       private Double amount;
}
