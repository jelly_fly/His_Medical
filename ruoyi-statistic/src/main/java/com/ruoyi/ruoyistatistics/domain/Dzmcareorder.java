package com.ruoyi.ruoyistatistics.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
@TableName("dzm_his_care_order")
public class Dzmcareorder {
       //收费总表
       @TableId
       @Excel(name = "序号",cellType = Excel.ColumnType.NUMERIC,sort = 1)
       private Integer id;// int(10) unsigned NOT NULL AUTO_INCREMENT,
       private Integer hospitalId;// int(10) unsigned DEFAULT '0',
       private Integer doctorId;// int(10) unsigned DEFAULT '0',
       @Excel(name = "患者ID",cellType = Excel.ColumnType.NUMERIC,sort =4)
       private Integer patientId;// int(10) unsigned DEFAULT '0',
       @Excel(name = "病历ID",cellType = Excel.ColumnType.NUMERIC,sort = 3)
       private Integer careHistoryId;// int(10) unsigned DEFAULT '0',
       @Excel(name = "挂号ID",cellType = Excel.ColumnType.NUMERIC,sort = 2)
       private Integer registrationId;// int(10) unsigned DEFAULT '0' COMMENT '挂号ID',
       private String orderCode;// varchar(64) DEFAULT NULL COMMENT '商户订单号',
       @Excel(name = "应付金额",sort =12)
       private Integer amount;// decimal(10,2) DEFAULT '0.00' COMMENT '应付金额',
       private Integer olPayPart;// decimal(10,2) DEFAULT '0.00' COMMENT '在线支付部分',
       private Integer typeId;// tinyint(1) unsigned DEFAULT '0' COMMENT '收费类型：0就诊处，1挂号处，2问答，3...',
       @Excel(name = "支付状态",readConverterExp = "0=未支付,1=已支付,2=确认收款,3=申请退款,4=已退款,5=部分支付,6=完成交易,7=部分退款",sort =11)
       private Integer status;// tinyint(1) unsigned DEFAULT '0' COMMENT '状态:0未支付，1已支付，2确认收款，3申请退款，4已退款,5部分支付,6完成交易（如：已发药），7部分退款',
       @JsonFormat(pattern = "yyyy-mm-dd", timezone = "GTM-8")
       @Excel(name = "看诊时间时间",sort =10)
       private Integer addtime;// int(10) unsigned DEFAULT '0' COMMENT '插入时间',
       private Integer opPlace;// tinyint(1) unsigned DEFAULT '0' COMMENT '操作地点：1售药，2查检项目，3附加费用，4挂号，，，，',

       //@TableField(exist = false) 注解加载bean属性上，表示当前属性不是数据库的字段，
       // 但在项目中必须使用，这样在新增等使用bean的时候，mybatis-plus就会忽略这个，不会报错。
       @TableField(exist = false)
       private long registrationNumber;// bigint(20) NOT NULL COMMENT '挂号编号',
       @TableField(exist = false)
       @Excel(name = "患者姓名",sort =5)
       private String name;// varchar(50) NOT NULL DEFAULT '患者姓名',
       @TableField(exist = false)
       @Excel(name = "患者电话",sort =8)
       private long mobile;// varchar(11) NOT NULL DEFAULT '' COMMENT '患者电话',
       @TableField(exist = false)
       @Excel(name = "患者性别",readConverterExp = "1=男,2=女",sort =6)
       private Integer sex;// tinyint(2) DEFAULT '0' COMMENT '患者性别1男2女',
       @TableField(exist = false)
       @JsonFormat(pattern = "yyyy-mm-dd", timezone = "GTM-8")
       @Excel(name = "患者出生日期",sort =7)
       private String birthday;// varchar(50) DEFAULT NULL, 患者年龄
       @TableField(exist = false)
       @Excel(name = "医生姓名",sort =9)
       private String truename;// varchar(20) NOT NULL DEFAULT '' COMMENT '用户个人资料真实姓名',
       @TableField(exist = false)
       private Integer createTime;// int(11) NOT NULL,就诊时间



}
