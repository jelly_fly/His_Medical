package com.ruoyi.ruoyistatistics.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@TableName("dzm_patient")
@Alias("dz")
public class Dzmpatient {
      //患者编号
      @Excel(name = "患者编号")
      private Integer id;
      //患者姓名
      @Excel(name = "患者姓名")
      private String name;
      //患者电话
      @Excel(name = "患者电话")
      private String mobile;
      //患者性别
      @Excel(name = "患者性别")
      private String sex;
      //患者身份证号
      @Excel(name = "患者身份证号")
      private String card;
      //支付方式
      @Excel(name = "支付方式")
      private String platform;
      //支付状态
      @Excel(name = "支付状态")
      private Integer status;
      //支付金额
      @Excel(name = "支付金额")
      private Double amount;
}
