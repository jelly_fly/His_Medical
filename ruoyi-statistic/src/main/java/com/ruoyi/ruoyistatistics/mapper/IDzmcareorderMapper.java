package com.ruoyi.ruoyistatistics.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmcareorder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
@DataSource(value = DataSourceType.SLAVE)
public interface IDzmcareorderMapper extends BaseMapper<Dzmcareorder> {
       List<Dzmcareorder> queryList(Dzmcareorder dzmcareorder);

       List<Dzmcareorder> orders(Dzmcareorder dzmcareorders);

       Dzmcareorder Patient(Integer id);

       List<Dzmcareorder> prescriPtion(Integer id);

       Integer upStatus(Dzmcareorder dzmHisCarePkg);

       Integer paytheFees(Dzmcareorder dzmHisCarePkg);

       Integer reiMburse(Dzmcareorder dzmHisCarePkg);

       List<Dzmcareorder> selectdzmPkg(Dzmcareorder dzmHisCarePkg);
}
