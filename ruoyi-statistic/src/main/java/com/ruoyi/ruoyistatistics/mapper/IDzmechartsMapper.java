package com.ruoyi.ruoyistatistics.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.ruoyistatistics.domain.Dzmecharts;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IDzmechartsMapper extends BaseMapper<Dzmecharts> {

    List<Dzmecharts> amount(Dzmecharts dzmecharts);

    List<Dzmecharts> sf(Dzmecharts dzmecharts);
}
