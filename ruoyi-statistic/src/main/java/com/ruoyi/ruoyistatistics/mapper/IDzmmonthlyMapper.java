package com.ruoyi.ruoyistatistics.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.ruoyistatistics.domain.Dzmmonthly;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IDzmmonthlyMapper extends BaseMapper<Dzmmonthly> {
        List<Dzmmonthly> ydtj(Dzmmonthly dzmmonthly);
}
