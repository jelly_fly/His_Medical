package com.ruoyi.ruoyistatistics.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmregistration;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@DataSource(value = DataSourceType.SLAVE)
@Mapper
public interface IDzmregistrationMapper extends BaseMapper<Dzmregistration> {
               List<Dzmregistration> Find(Dzmregistration dzmregistration);
}
