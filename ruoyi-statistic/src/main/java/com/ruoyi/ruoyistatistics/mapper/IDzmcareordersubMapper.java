package com.ruoyi.ruoyistatistics.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.ruoyistatistics.domain.Dzmcareordersub;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IDzmcareordersubMapper extends BaseMapper<Dzmcareordersub> {
    List<Dzmcareordersub> sub(Dzmcareordersub dzmcareordersub);
}
