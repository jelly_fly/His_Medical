package com.ruoyi.ruoyistatistics.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmregistration;
import com.ruoyi.ruoyistatistics.mapper.IDzmregistrationMapper;
import com.ruoyi.ruoyistatistics.service.IDzmregistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class IDzmregistrationServiceimpl extends ServiceImpl<IDzmregistrationMapper, Dzmregistration> implements IDzmregistrationService {
    @Autowired
    private IDzmregistrationMapper mapper;

    @Override
    public List<Dzmregistration> Find(Dzmregistration dzmregistration) {
        return mapper.Find(dzmregistration);
    }
}
