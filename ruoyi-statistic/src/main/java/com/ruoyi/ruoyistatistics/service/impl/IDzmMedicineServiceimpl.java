package com.ruoyi.ruoyistatistics.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmmedicine;
import com.ruoyi.ruoyistatistics.mapper.IDzmmedicineMapper;
import com.ruoyi.ruoyistatistics.service.IDzmMedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class IDzmMedicineServiceimpl extends ServiceImpl<IDzmmedicineMapper, Dzmmedicine> implements IDzmMedicineService {
    @Autowired
    private IDzmmedicineMapper mapper;

    @Override
    public List<Dzmmedicine> all(Dzmmedicine dzmmedicine) {
        return mapper.all(dzmmedicine);
    }
}
