package com.ruoyi.ruoyistatistics.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmcareordersub;
import com.ruoyi.ruoyistatistics.mapper.IDzmcareordersubMapper;
import com.ruoyi.ruoyistatistics.service.IDzmcareordersubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class IDzmcareordersubServiceimpl extends ServiceImpl<IDzmcareordersubMapper, Dzmcareordersub> implements IDzmcareordersubService {
    @Autowired
    private IDzmcareordersubMapper mapper;

    @Override
    public List<Dzmcareordersub> sub(Dzmcareordersub dzmcareordersub) {
        return mapper.sub(dzmcareordersub);
    }
}
