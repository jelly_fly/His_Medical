package com.ruoyi.ruoyistatistics.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmecharts;
import com.ruoyi.ruoyistatistics.mapper.IDzmechartsMapper;
import com.ruoyi.ruoyistatistics.service.IDzmechartsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class IDzmeachrtsServiceimpl extends ServiceImpl<IDzmechartsMapper, Dzmecharts> implements IDzmechartsService {
     @Autowired
     private IDzmechartsMapper mapper;

    @Override
    public List<Dzmecharts> amount(Dzmecharts dzmecharts) {
        return mapper.amount(dzmecharts);
    }

    @Override
    public List<Dzmecharts> sf(Dzmecharts dzmecharts) {
        return mapper.sf(dzmecharts);
    }
}
