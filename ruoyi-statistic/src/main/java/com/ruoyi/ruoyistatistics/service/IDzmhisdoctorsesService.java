package com.ruoyi.ruoyistatistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ruoyistatistics.domain.Dzmhisdoctorses;

import java.util.List;

public interface IDzmhisdoctorsesService extends IService<Dzmhisdoctorses> {
         List<Dzmhisdoctorses> doc(Dzmhisdoctorses dzmhisdoctor);
}
