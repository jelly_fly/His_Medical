package com.ruoyi.ruoyistatistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ruoyistatistics.domain.Dzmregistration;

import java.util.List;

public interface IDzmregistrationService extends IService<Dzmregistration> {
         List<Dzmregistration> Find(Dzmregistration dzmregistration);
}
