package com.ruoyi.ruoyistatistics.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmhisdoctorses;
import com.ruoyi.ruoyistatistics.mapper.IDzmhisdoctorsesMapper;
import com.ruoyi.ruoyistatistics.service.IDzmhisdoctorsesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class IDzmhisdoctorsesServiceimpl extends ServiceImpl<IDzmhisdoctorsesMapper, Dzmhisdoctorses> implements IDzmhisdoctorsesService {
    @Autowired
    private IDzmhisdoctorsesMapper mapper;

    @Override
    public List<Dzmhisdoctorses> doc(Dzmhisdoctorses dzmhisdoctor) {
        return mapper.doc(dzmhisdoctor);
    }
}
