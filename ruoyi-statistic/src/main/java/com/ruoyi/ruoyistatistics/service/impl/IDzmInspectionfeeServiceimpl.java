package com.ruoyi.ruoyistatistics.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzminspectionfee;
import com.ruoyi.ruoyistatistics.mapper.IDzmInspectionfeeMapper;
import com.ruoyi.ruoyistatistics.service.IDzmInspectionfeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class IDzmInspectionfeeServiceimpl extends ServiceImpl<IDzmInspectionfeeMapper,Dzminspectionfee> implements IDzmInspectionfeeService {
    @Autowired
    private IDzmInspectionfeeMapper mapper;

    @Override
    public List<Dzminspectionfee> sles(Dzminspectionfee dzminspectionfee) {
        return mapper.sles(dzminspectionfee);
    }
}
