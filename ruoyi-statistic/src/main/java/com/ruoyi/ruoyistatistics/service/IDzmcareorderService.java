package com.ruoyi.ruoyistatistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ruoyistatistics.domain.Dzmcareorder;

import java.util.List;

public interface IDzmcareorderService extends IService<Dzmcareorder> {
    List<Dzmcareorder> queryList(Dzmcareorder dzmcareorder);

    List<Dzmcareorder> orders(Dzmcareorder dzmcareorders);

    Dzmcareorder Patient(Integer id);

    List<Dzmcareorder> prescriPtion(Integer id);

    Integer upStatus(Dzmcareorder dzmHisCarePkg);

    Integer paytheFees(Dzmcareorder dzmHisCarePkg);

    Integer reiMburse(Dzmcareorder dzmHisCarePkg);

    List<Dzmcareorder> selectdzmPkg(Dzmcareorder dzmHisCarePkg);

}
