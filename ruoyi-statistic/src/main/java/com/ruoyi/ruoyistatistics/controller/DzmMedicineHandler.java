package com.ruoyi.ruoyistatistics.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.ruoyistatistics.domain.Dzmmedicine;
import com.ruoyi.ruoyistatistics.domain.Dzmpatient;
import com.ruoyi.ruoyistatistics.service.IDzmMedicineService;
import com.ruoyi.ruoyistatistics.service.impl.IDzmMedicineServiceimpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/his/dzmmedicine")
public class DzmMedicineHandler extends BaseController {
    @Autowired
    private IDzmMedicineService service;

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @GetMapping("/all")
    public Object list(Dzmmedicine dzmmedicine){
        startPage();
        List<Dzmmedicine> list = service.all(dzmmedicine);
        return getDataTable(list);
    }

    //导出
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @Log(title = "定时任务", businessType = BusinessType.EXPORT)
    @PostMapping("/exports")
    public void export(HttpServletResponse response,Dzmmedicine dzmmedicine)
    {
        List<Dzmmedicine> list = service.all(dzmmedicine);
        ExcelUtil<Dzmmedicine> util = new ExcelUtil<Dzmmedicine>(Dzmmedicine.class);
        util.exportExcel(response, list, "定时任务");
    }
}
