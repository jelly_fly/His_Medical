package com.ruoyi.ruoyistatistics.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.ruoyistatistics.domain.Dzmcareorder;
import com.ruoyi.ruoyistatistics.service.IDzmcareorderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/his/dzmcareorder")
public class DzmcareorderHandler extends BaseController {
       @Autowired
      private IDzmcareorderService service;

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @GetMapping("/orders")
    public Object orders(Dzmcareorder dzmcareorders){
        startPage();
        List<Dzmcareorder> orders = service.orders(dzmcareorders);
        return getDataTable(orders);
    }

    //患者信息
    @PreAuthorize("@ss.hasPermi('pharmacist') || @ss.hasPermi('chief:pharmacist')")
    @GetMapping("/patient/{id}")
    public Object Patient(@PathVariable Integer id) {
        return service.Patient(id);
    }

    //处方列表信息
    @PreAuthorize("@ss.hasPermi('pharmacist') || @ss.hasPermi('chief:pharmacist')")
    @GetMapping("/prescription/{id}")
    public Object PrescriPtion(@PathVariable Integer id){
        return service.prescriPtion(id);
    }

    @PreAuthorize("@ss.hasPermi('pharmacist') || @ss.hasPermi('chief:pharmacist')")
    @GetMapping("/list")
    public TableDataInfo list(Dzmcareorder pkg) {
        startPage();
        List<Dzmcareorder> list = service.queryList(pkg);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('pharmacist') || @ss.hasPermi('chief:pharmacist')")
    @PutMapping("/upStatus")
    public AjaxResult upStatus(@Validated @RequestBody  Dzmcareorder dzmHisCarePkg) {
        startPage();
        return toAjax(service.upStatus(dzmHisCarePkg));
    }

    @PreAuthorize("@ss.hasPermi('pharmacist') || @ss.hasPermi('chief:pharmacist')")
    @PutMapping("/paytheFees")
    public AjaxResult paytheFees(@Validated @RequestBody Dzmcareorder dzmHisCarePkg) {
        startPage();
        return toAjax(service.paytheFees(dzmHisCarePkg));
    }

    @PreAuthorize("@ss.hasPermi('pharmacist') || @ss.hasPermi('chief:pharmacist')")
    @PutMapping("/reiMburse")
    public AjaxResult reiMburse(@Validated @RequestBody  Dzmcareorder dzmHisCarePkg) {
        startPage();
        return toAjax(service.reiMburse(dzmHisCarePkg));
    }

    //导出
    @PreAuthorize("@ss.hasPermi('pharmacist') || @ss.hasPermi('chief:pharmacist')")
    @Log(title = "定时任务", businessType = BusinessType.EXPORT)
    @PostMapping("/ports")
    public void export(HttpServletResponse response, Dzmcareorder dzmcareorder)
    {
        List<Dzmcareorder> list = service.orders(dzmcareorder);
        ExcelUtil<Dzmcareorder> util = new ExcelUtil<Dzmcareorder>(Dzmcareorder.class);
        util.exportExcel(response, list, "定时任务");
    }


}
