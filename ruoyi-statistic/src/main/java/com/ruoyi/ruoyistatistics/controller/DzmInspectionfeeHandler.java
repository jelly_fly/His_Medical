package com.ruoyi.ruoyistatistics.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.ruoyistatistics.domain.Dzminspectionfee;
import com.ruoyi.ruoyistatistics.service.IDzmInspectionfeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/his/dzminspectionfee")
public class DzmInspectionfeeHandler extends BaseController {
          @Autowired
        private IDzmInspectionfeeService service;

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
        @GetMapping("/sles")
        public Object sles(Dzminspectionfee dzminspectionfee){
            startPage();
            List<Dzminspectionfee> sles = service.sles(dzminspectionfee);
            return getDataTable(sles);
        }

    //导出
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @Log(title = "定时任务", businessType = BusinessType.EXPORT)
    @PostMapping("/port")
    public void export(HttpServletResponse response, Dzminspectionfee dzminspectionfee)
    {
        List<Dzminspectionfee> list = service.sles(dzminspectionfee);
        ExcelUtil<Dzminspectionfee> util = new ExcelUtil<Dzminspectionfee>(Dzminspectionfee.class);
        util.exportExcel(response, list, "定时任务");
    }
}
