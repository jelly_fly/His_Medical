package com.ruoyi.ruoyistatistics.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.ruoyistatistics.domain.Dzmregisteredfee;
import com.ruoyi.ruoyistatistics.service.IDzmRegisteredFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/his/dzmregisteredfee")
public class DzmRegisteredFeeHandler extends BaseController {
      @Autowired
      private IDzmRegisteredFeeService service;

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
      @GetMapping("/findAll")
      public Object findAll(Dzmregisteredfee dzmregisteredfee){
          startPage();
          List<Dzmregisteredfee> findAll = service.findAll(dzmregisteredfee);
          return getDataTable(findAll);
          }
      }
