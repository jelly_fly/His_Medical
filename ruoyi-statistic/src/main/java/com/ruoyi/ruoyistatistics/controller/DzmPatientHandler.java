package com.ruoyi.ruoyistatistics.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.ruoyistatistics.service.IDzmPatientService;
import com.ruoyi.ruoyistatistics.domain.Dzmpatient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;



@RestController
@RequestMapping("/his/dzmpatient")
public class DzmPatientHandler extends BaseController {
    @Autowired
    private IDzmPatientService service;

    //患者缴费查询
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @GetMapping("/list")
    public Object list(Dzmpatient dzmpatient){
        startPage();
        List<Dzmpatient> list = service.sel(dzmpatient);
        return getDataTable(list);
    }

    //导出
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @Log(title = "定时任务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response,Dzmpatient dzmpatient)
    {
        List<Dzmpatient> list = service.sel(dzmpatient);
        ExcelUtil<Dzmpatient> util = new ExcelUtil<Dzmpatient>(Dzmpatient.class);
        util.exportExcel(response, list, "定时任务");
    }
}
