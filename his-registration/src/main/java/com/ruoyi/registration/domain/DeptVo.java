package com.ruoyi.registration.domain;

import lombok.Data;

@Data
public class DeptVo {
    private Integer did;
    private String departmentName;
    private Integer schedulingType;
    private String  registeredfeeName;
    private String trueName;
    private Double registeredfeeFee;
    private Integer physicianid;
    private  Integer regId;
    private Integer patientId;
    private String name;
    private Integer sex;
    private  String caseCode;
    private String birthday;
    private String idCard;
    private Integer typeId;
    private String registrationNumber;
    private  long createTime;
    private Integer registrationId;
    private Integer subsectionType ;//医生排班时段;

}
