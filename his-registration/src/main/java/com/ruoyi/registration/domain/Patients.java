package com.ruoyi.registration.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
public class Patients {
    private String name;//患者姓名
    private Integer sex;//患者性别
    private String  birthday;//患者出生日期
    private String idCard; //患者身份证号
    private String  mobile;// varchar(11) NOT NULL DEFAULT '' COMMENT '患者电话',
    private String  mobile1;//char(11) DEFAULT NULL,
    private Integer isFinal;// tinyint(4) DEFAULT '0' COMMENT '是否完善信息，0否1已完善',
    private String  address;// varchar(120) DEFAULT NULL COMMENT '地址信息',
    private String  createTime;// int(10) NOT NULL DEFAULT '0' COMMENT '注册时间',
    private String   allergyInfo;//varchar(100) DEFAULT NULL COMMENT '过敏信息',

}
