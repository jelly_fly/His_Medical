package com.ruoyi.registration.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.core.domain.entity.DzmHisScheduling;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.registration.mapper.ISchedulingMapper;
import com.ruoyi.registration.service.ISchedulingService;
import com.ruoyi.system.service.impl.SysUserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@DataSource(DataSourceType.SLAVE)
@Service("Scheduling")
public class SchedulingServiceImpl implements ISchedulingService {

@Autowired
private ISchedulingMapper mapper;

    private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);

    public String importDzmHisScheduling(List<DzmHisScheduling> userList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(userList) || userList.size() == 0)
        {
            throw new ServiceException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
//        String password = configService.selectConfigByKey("sys.user.initPassword");
        for (DzmHisScheduling user : userList)
        {
            try
            {   successNum++;
                System.out.println(user);
                insert(user);

            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" +"导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    @Override
    public Integer insert(DzmHisScheduling scheduling) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        scheduling.setSubsectionDate(sdf.format(Date.parse(scheduling.getSubsectionDate())));

        System.out.println(scheduling.getSubsectionDate());
        return mapper.insert(scheduling);
    }

    @Override
    public List<DzmHisScheduling> list(DzmHisScheduling dzmHisScheduling) {
        return mapper.list(dzmHisScheduling);
    }

    @Override
    public List<DzmHisScheduling> alllist(DzmHisScheduling dzmHisScheduling) {
        return mapper.alllist(dzmHisScheduling);
    }

    @Override
    public List<DzmHisScheduling> wdpb(DzmHisScheduling dzmHisScheduling) {
        return mapper.wdpb(dzmHisScheduling);
    }

    @Override
    public DzmHisScheduling getScheduling(Integer scheduling_id) {
        return mapper.getScheduling(scheduling_id);
    }

    @Override
    public int upScheduling(DzmHisScheduling scheduling) {
        return mapper.upScheduling(scheduling);
    }
}
