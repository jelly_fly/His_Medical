package com.ruoyi.registration.service;

import com.ruoyi.registration.domain.HzVo;
import com.ruoyi.registration.domain.Patients;

import java.util.List;

public interface IHzService {
    public List<HzVo> list(HzVo hzVo);
    public Integer  insertPatients(Patients patients);
}
