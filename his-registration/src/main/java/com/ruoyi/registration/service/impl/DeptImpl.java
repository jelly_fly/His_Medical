package com.ruoyi.registration.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.registration.domain.DeptVo;
import com.ruoyi.registration.mapper.IDeptMapper;
import com.ruoyi.registration.service.IDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class DeptImpl implements IDeptService {
    @Autowired
    private IDeptMapper mapper;
    @Override
    public List<DeptVo> list() {
        return mapper.list();
    }

    @Override
    public List<DeptVo> bg(DeptVo deptVo) {
        return mapper.bg(deptVo);
    }

    @Override
    public Integer qrgh(DeptVo deptVo) {
        return mapper.qrgh(deptVo);
    }

    @Override
    public int addHistory(DeptVo deptVo) {
        return mapper.addHistory(deptVo);
    }

    @Override
    public int addPkg(DeptVo deptVo) {
        return mapper.addPkg(deptVo);
    }

    @Override
    public int upsdatecheduling(DeptVo deptVo) {
        return mapper.upsdatecheduling(deptVo);
    }


}
