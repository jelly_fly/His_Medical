package com.ruoyi.registration.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.registration.domain.HzVo;
import com.ruoyi.registration.domain.Patients;
import com.ruoyi.registration.mapper.IHzMapper;
import com.ruoyi.registration.service.IHzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@DataSource(DataSourceType.SLAVE)
@Service
public class HzServiceImpl implements IHzService {
    @Autowired
    private IHzMapper mapper;
    @Override
    public List<HzVo> list(HzVo hzVo) {
        return mapper.list(hzVo);
    }

    @Override
    public Integer insertPatients(Patients patients) {
        return mapper.insertPatients(patients);
    }
}
