package com.ruoyi.registration.service;

import com.ruoyi.common.core.domain.entity.DzmHisScheduling;

import java.util.List;

public interface ISchedulingService {
    public String importDzmHisScheduling(List<DzmHisScheduling> userList, Boolean isUpdateSupport, String operName);
    public Integer insert(DzmHisScheduling scheduling);
    public List<DzmHisScheduling> list(DzmHisScheduling dzmHisScheduling);
    public List<DzmHisScheduling> alllist(DzmHisScheduling dzmHisScheduling);
    public List<DzmHisScheduling> wdpb(DzmHisScheduling dzmHisScheduling);
    public DzmHisScheduling getScheduling(Integer scheduling_id);

    int upScheduling(DzmHisScheduling scheduling);
}
