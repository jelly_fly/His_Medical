import request from '@/utils/request'

// 查询门诊挂号列表
export function listRegistration(query) {
  return request({
    url: '/his/medicallist/list',
    method: 'get',
    params: query
  })
}

// 查询退号信息详细
export function getRegistration(registrationId) {
  return request({
    url: '/his/medicallist/backno/' + registrationId,
    method: 'get'
  })
}

// 查询患者信息详细
export function getPersonalDetails(registrationId) {
  return request({
    url: '/his/personalDetails/personbackno/' + registrationId,
    method: 'get'
  })
}

// 查询订单信息详细
export function getOrderBackno(registrationId) {
  return request({
    url: '/his/personalDetails/orderbackno/' + registrationId,
    method: 'get'
  })
}

// 查询挂号信息详细
export function getRegistrationBackno(registrationId) {
  return request({
    url: '/his/personalDetails/registrationbackno/' + registrationId,
    method: 'get'
  })
}

// 修改门诊挂号转态
export function updateRegistration(registrationId) {
  return request({
    url: '/his/medicallist/update/' + registrationId,
    method: 'get',
  })
}





// 新增门诊挂号
export function addRegistration(data) {
  return request({
    url: '/his/medicallist',
    method: 'post',
    data: data
  })
}

// 删除门诊挂号
export function delRegistration(registrationId) {
  return request({
    url: '/his/medicallist/' + registrationId,
    method: 'delete'
  })
}
