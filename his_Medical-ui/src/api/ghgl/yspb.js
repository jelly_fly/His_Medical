
import request from '@/utils/request'

// 查询我的排班列表
export function listScheduling(query) {
  return request({
    url: '/ghgl/yspb/alllist',
    method: 'get',
    params: query
  })
}

// 查询我的排班详细
export function getScheduling(schedulingId) {
  return request({
    url: '/ghgl/yspb/getScheduling?scheduling_id=' + schedulingId,
    method: 'get'
  })
}

// 新增我的排班
export function addScheduling(data) {
  return request({
    url: '/system/scheduling',
    method: 'post',
    data: data
  })
}

// 修改我的排班
export function updateScheduling(data) {
  return request({
    url: '/system/scheduling',
    method: 'put',
    data: data
  })
}

// 删除我的排班
export function delScheduling(schedulingId) {
  return request({
    url: '/system/scheduling/' + schedulingId,
    method: 'delete'
  })
}
// 修改医生排班
export function updateUser(data) {
    return request({
      url: '/ghgl/yspb/upscheduling',
      method: 'put',
      data: data
    })
}