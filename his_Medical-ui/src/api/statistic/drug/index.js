/*
 * @Author: chaihaoran 1605746813@qq.com
 * @Date: 2022-06-22 09:36:43
 * @LastEditors: chaihaoran 1605746813@qq.com
 * @LastEditTime: 2022-07-11 09:09:22
 * @FilePath: \his_Medical-ui\src\api\statistic\drug\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from '@/utils/request'

// 查询处方明细列表
export function listPost(query) {
  return request({
    url: '/his/dzmmedicine/all',
    method: 'get',
    params: query
  })
}
// 查询药品排行列表
export function listPosts(query) {
  return request({
    url: '/his/dzmcareordersub/sub',
    method: 'get',
    params: query
  })
}
