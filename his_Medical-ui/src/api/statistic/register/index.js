/*
 * @Author: chaihaoran 1605746813@qq.com
 * @Date: 2022-07-11 17:25:05
 * @LastEditors: chaihaoran 1605746813@qq.com
 * @LastEditTime: 2022-07-18 21:55:00
 * @FilePath: \his_Medical-ui\src\api\statistic\register\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from '@/utils/request'

// 查询发药列表
export function fylistPost(query) {
    return request({
        url: '/his/dzmcareorder/list',
        method: 'get',
        params: query
    })
}

// 查询患者信息
export function hzpost(id) {
    return request({
        url: '/his/dzmcareorder/patient/' + id,
        method: 'get'
    })
}

//查询处方信息
export function cfpost(id) {
    return request({
        url: '/his/dzmcareorder/prescription/' + id,
        method: 'get'
    })
}

//确认发药完结束状态
export function updatePost(data) {
    return request({
        url: '/his/dzmcareorder/upStatus',
        method: 'put',
        data: data
    })
}

//缴费改成发药状态
export function paythePost(data) {
    return request({
        url: '/his/dzmcareorder/paytheFees',
        method: 'put',
        data: data
    })
}

//发药改成退款状态
export function reimbPost(data) {
    return request({
        url: '/his/dzmcareorder/reiMburse',
        method: 'put',
        data: data
    })
}