/*
 * @Author: chaihaoran 1605746813@qq.com
 * @Date: 2022-07-05 10:51:27
 * @LastEditors: chaihaoran 1605746813@qq.com
 * @LastEditTime: 2022-07-12 09:32:37
 * @FilePath: \his_Medical-ui\src\api\statistic\workload\idnex.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from '@/utils/request'

//门诊挂号统计
export function listPost(query) {
    return request({
      url: '/his/dzmregistration/find',
      method: 'get',
      params: query
    })
  }

//门诊处方统计 
export function listPosts(query) {
    return request({
      url: '/his/dzmcareorder/orders',
      method: 'get',
      params: query
    })
  }

//门诊费用统计  
export function listPostse(query) {
    return request({
      url: '/his/dzmhisdoctor/doc',
      method: 'get',
      params: query
    })
  }