/*
 * @Author: chaihaoran 1605746813@qq.com
 * @Date: 2022-07-11 17:25:05
 * @LastEditors: chaihaoran 1605746813@qq.com
 * @LastEditTime: 2022-07-18 15:08:15
 * @FilePath: \his_Medical-ui\src\api\statistic\monthly\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

import request from '@/utils/request'

export function ydtj(query) {
    return request({
      url: '/his/dzmmonthly/ydtj',
      method: 'get',
      params: query,
      success:function(res){
      }
    })
  }

