import request from '@/utils/request'

// 查询项目列表
export function listPost(query) {
  return request({
    url: '/his/dzminspectionfee/sles',
    method: 'get',
    params: query
  })
}
