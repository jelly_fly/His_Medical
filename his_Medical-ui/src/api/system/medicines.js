

import request from '@/utils/request';


// 查询药品信息列表
export function medilist(query) {
  return request({
    url: '/medicines/querylist',
    method: 'get',
    params: query
  })
}
//删除药品
export function delmedicines(medicinesId) {
  return request({
    url: '/medicines/del?medicinesId'+('='+medicinesId),
    method: 'get'
  })
}
//导入药品
export function importTemplate() {
  return request({
    url: '/medicines/importTemplate',
    method: 'get'
  })
}

